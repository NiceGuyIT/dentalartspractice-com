+++
title = "Pago"
#description = "Métodos de pago"
description = ""
lastmod = "2023-05-31T00:00:00"
bg_image = "images/bg/page-title.jpg"
layout = "cherry"
draft = false

[menu.main]
name = "Pago"
weight = 45

[menu.footer]
name = "Pago"
weight = 45

# https://webapps.stackexchange.com/questions/12125/how-to-get-a-link-to-a-google-translate-translation
# Translate URL is the URL to view the page in Google Translate.
translate_url = "https://dentalartspractice-com.translate.goog/es/payment/?_x_tr_sl=en&_x_tr_tl=es&_x_tr_hl=es&_x_tr_pto=wapp"

# Translate text is the text to display in the button.
translate_text = "Esta página no está disponible en español. Haga clic aquí para ver esta página en Google Translate."
+++
