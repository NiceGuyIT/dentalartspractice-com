+++
title = "Servicios"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "service"
draft = false

[menu.main]
name = "Servicios"
weight = 20

[menu.footer]
name = "Servicios"
weight = 20

############################## About Service ##############################
[about]
enable = false
title = ""
content = """
"""
image = "images/company/company-group-pic.jpg"

# Should be no more than 3 features services.
[featured_service]
enable = false

############################## Featured Service ###########################
[[featured_service.service_item]]
name = "Featured Service 1"
icon = "magic"
color = "primary"
content = ""

[[featured_service.service_item]]
name = "Featured Service 2"
icon = "image"
color = "primary-dark"
content = ""

[[featured_service.service_item]]
name = "Featured Service 3"
icon = "leaf"
color = "primary-darker"
content = ""


############################## Service ####################################
[service]
enable = true
title = "Nuestros servicios"
description = ""

[[service.service_item]]
icon = "magic"
icon_color = "#0000cd"
category = "Implantes dentales"
name = "Implantes dentales"
content = """
Los implantes dentales son una solución permanente y atractiva para reemplazar los dientes extraídos o perdidos.
"""

[[service.service_item]]
icon = "teeth"
icon_color = "#ad76ff"
category = "Higiene dental"
name = "Higiene dental"
content = """
Mientras esté en nuestra oficina, nos aseguramos de que reciba el más alto nivel de servicio y nos aseguramos de que
nuestro trabajo dental sea de la más alta calidad.
"""

[[service.service_item]]
icon = "image"
icon_color = "#ffd700"
name = "Cosmética"
category = "Carillas"
content = """
Carillas, Blanqueamiento, Adhesión y empastes blancos, Inlays y Onlays
"""

[[service.service_item]]
icon = "cubes"
icon_color = "#800080"
name = "Restaurativa"
category = "Puentes"
content = """
Puentes, Coronas, Dentaduras, Endodoncias
"""

[[service.service_item]]
icon = "tooth"
icon_color = "#87cefa"
name = "Periodoncia"
category = "Arestin"
content = """
Láser, biopsia, injerto óseo, aumento de cresta, aumento de senos paranasales, exposición canina, alargamiento de
corona, frenectomía, injerto gingival/de encía
"""

[[service.service_item]]
icon = "mortar-pestle"
icon_color = "#778899"
name = "Ortodoncia"
category = "Clear Correct"
content = """
ClearCorrect, Sparks, Clear Aligners, Night Guards, TMJ
"""

[[service.service_item]]
icon = "stethoscope"
icon_color = "#7fe1a6"
name = "Endodoncista"
category = ""
content = """
Endodoncia Anterior, Bicúspide y Molares, Retratamiento de Endodoncias, Apicectomías, Desbridamiento Pulpar (Abierto y Med)
"""


############################## Call to Action #############################
[cta]
enable = true

+++
