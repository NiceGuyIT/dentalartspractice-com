+++
title = "Conozca a nuestro equipo"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "staff"
draft = false

[menu.main]
name = "Personal"
weight = 40

[menu.footer]
name = "Personal"
weight = 40


[staff]
enable = true

[[staff.office]]
image = "images/staff/joann-garcia-office-manager-800x800.jpg"
weight = 10
name = "JoAnn Garcia"
title = "Gerente de oficina"
content = """
Hola, mi nombre es JoAnn. Soy el gerente de la oficina aquí en Dental Arts Practice. Trabajo en Dental FieId desde hace
26 años. Cuando comencé a trabajar con pacientes, asumí el puesto de Recepcionista Dental, donde progresé en facturación
y luego asumí la responsabilidad de ser Gerente de Oficina. Además de administrar un negocio, disfruto de las caminatas
y el levantamiento de pesas en mi tiempo libre. De vez en cuando, los fines de semana, también disfruto ir a conciertos
con mis hermanos y escuchar música Rock and Roll. Lo más importante es que mi momento favorito del día es el tiempo que
paso con mi prometido, mis dos hijos y mi perro (FIF).
"""

[[staff.office]]
image = "images/staff/marie-barton-treatment-coordinator-800x800.jpg"
weight = 20
name = "Marie Barton"
title = "Coordinadora de tratamiento"
content = """
Hola, mi nombre es Marie, crecí en Los Ángeles, CA. Comencé a trabajar en el campo de la odontología en 1988. Obtuve mi
experiencia trabajando junto a un médico, como asistente dental. Cuando estoy lejos de mis amigos en la oficina;
Disfruto pasar tiempo con mi esposo, 4 hijos y 2 nietos. Los fines de semana, disfruto ir a partidos de fútbol y
relajarme bajo el cálido sol.
"""

[[staff.office]]
image = "images/staff/nadia-stopani-insurance-biller-800x800.jpg"
weight = 30
name = "Nadia Stopani"
title = "Facturador de seguros"
content = """
Hola, mi nombre es Nadia, crecí en el este de Los Ángeles. He trabajado en el campo dental durante 7 años y he
disfrutado trabajando y preparando café por la mañana para el Dr. Nazari. En mi tiempo libre cuando no estoy facturando,
me encanta hacer reír a mis compañeros de trabajo y dar vida a la oficina tranquila. Por último, mi orgullo y alegría
son mis 3 hijos, mis 2 nietos y mi tercer nieto en camino. ¡¡¡BUENOS DODGERS !!!
"""

[[staff.office]]
image = "images/staff/monica-paredes-dental-assistant-800x800.jpg"
weight = 40
name = "Monica Paredes"
title = "Ayudante de dentista"
content = """
Hola, mi nombre es Monica Paredes. Soy asistente dental y trabajo en el campo dental desde hace 26 años. Me encanta y
disfruto ayudar al Dr. Nazari con todos los pacientes que nos visitan aquí en Dental Arts Practice. Cuando no estoy
trabajando, me encanta pasar tiempo con mi hijo y mis 2 nietos y el resto de mi familia. ¡Mi pasatiempo favorito es
llevar a mis nietos a la playa y ponerme bajo el sol!
"""

[[staff.office]]
image = "images/staff/benjamin-ceja-dental-assistant-800x800.jpg"
weight = 50
name = "Benjamin Ceja"
title = "Ayudante de dentista"
content = """
Hola, me llamo Benjamín, pero me llaman galleta para abreviar. Me crié en Oxnard y soy nuevo en el campo de la
odontología. Trabajo con los compañeros de trabajo más increíbles y enérgicos, realmente son los mejores. No cambiaría
eso por nada. Aparte de trabajar, me gusta pasar los días con mi esposa y 4 perros. En mis días libres, me gusta
escuchar música, jugar videojuegos y vivir aventuras con mi familia.
"""

[[staff.office]]
image = "images/staff/wendy-pena-rdh-800x800.jpg"
weight = 70
name = "Wendy Pena"
title = "Higienista dental registrado"
content = """
Hola, mi nombre es Wendy Pena. Me gradué de San Joaquin Valley College. Soy licenciada en Higiene Dental. Ha sido un
gran honor unirme al Dr. Nazari y su equipo. Disfruto principalmente trabajar con colegas profesionales de la salud
dental que comparten mi misma pasión por brindar atención al paciente de la mejor calidad. Comencé como asistente dental
en 2019, y ahora me complace ser higienista dental, asegurándome de que los pacientes siempre tengan una experiencia
cómoda y positiva. Me apasiona tratar a pacientes sensibles a sus necesidades dentales. Me considero un aprendiz de por
vida.

Fuera de la oficina, disfruto pasar tiempo con mi familia y salir y buscar cosas aventureras para hacer, desde viajes
hasta explorar nuevos lugares.
"""

[[staff.office]]
image = "images/staff/jonathan-king-800x800.jpg"
weight = 80
name = "Jonathan King"
title = "Higienista dental registrado"
content = """
Hola, soy Jonathan. Practico la higiene dental desde 2016, pero llevo 16 años en la industria dental. Mi enfoque principal
como higienista dental es brindar tratamiento periodontal y educar a los pacientes sobre la salud bucal. Lo que me llevó
a la odontología fueron los miembros de mi familia. Muchos de los miembros de mi familia están en el campo dental y
crearon una impresión duradera en mí para brindar atención dental. Mi mayor logro es cuando un paciente que regresa
valora su salud oral a través de visitas constantes y atención domiciliaria de rutina.
"""


[cta]
enable = true

+++
