+++
title = "Condiciones de uso"
description = ""
subtitle = ""
lastmod = "2021-10-09T00:00:00"
bg_image = "images/bg/page-title.jpg"
draft = false

[menu.footer]
name = "Condiciones de uso"
weight = 200

+++

# Condiciones de uso

El sitio web de {{< param "company.name" >}} ubicado en {{< param "company.name" >}} es una obra protegida por derechos de
autor que pertenece a {{< param "company.name" >}}. Ciertas características del Sitio pueden estar sujetas a pautas,
términos o reglas adicionales, que se publicarán en el Sitio en relación con dichas características.

Todos estos términos, pautas y reglas adicionales se incorporan por referencia en estos Términos.

Estos Términos de uso describen los términos y condiciones legalmente vinculantes que supervisan su uso del Sitio. AL
INGRESAR AL SITIO, USTED CUMPLE ESTOS TÉRMINOS y declara que tiene la autoridad y la capacidad para aceptar estos
Términos. USTED DEBE TENER AL MENOS 18 AÑOS PARA ACCEDER AL SITIO. SI NO ESTÁ DE ACUERDO CON TODAS LAS PROVISIONES DE
ESTOS TÉRMINOS, NO INICIE SESIÓN NI USE EL SITIO.

Estos términos requieren el uso de arbitraje (Sección 10.2) de forma individual para resolver disputas y también limitan
los recursos disponibles para usted en caso de una disputa.

## Acceso al sitio

**Sujeto a estos Términos.** La Compañía le otorga una licencia limitada, revocable, no transferible y no exclusiva para
acceder al Sitio únicamente para su uso personal y no comercial.

**Ciertas restricciones.** Los derechos aprobados para usted en estos Términos están sujetos a las siguientes
restricciones: (a) no venderá, alquilará, arrendará, transferirá, cederá, distribuirá, alojará ni explotará
comercialmente el Sitio; (b) no podrá cambiar, realizar trabajos derivados, desensamblar, realizar una compilación
inversa o realizar ingeniería inversa en ninguna parte del Sitio; (c) no deberá acceder al Sitio para crear un sitio web
similar o competitivo; y (d) salvo que se indique expresamente en el presente, ninguna parte del Sitio puede copiarse,
reproducirse, distribuirse, republicarse, descargarse, mostrarse, publicarse o transmitirse de ninguna forma o por
ningún medio a menos que se indique lo contrario, cualquier versión, actualización o actualización futura cualquier otra
adición a la funcionalidad del Sitio estará sujeta a estos Términos. Todos los avisos de derechos de autor y otros
avisos de propiedad en el Sitio deben conservarse en todas las copias del mismo.

La Compañía se reserva el derecho de cambiar, suspender o cesar el Sitio con o sin previo aviso. Aprobó que la Compañía
no será responsable ante usted ni ante ningún tercero por ningún cambio, interrupción o terminación del Sitio o de
cualquier parte.

**Sin soporte ni mantenimiento.** Usted acepta que la Compañía no tendrá la obligación de brindarle ningún soporte en
relación con el Sitio.

Excluyendo cualquier Contenido de usuario que pueda proporcionar, usted es consciente de que todos los derechos de
propiedad intelectual, incluidos los derechos de autor, patentes, marcas comerciales y secretos comerciales, en el Sitio
y su contenido son propiedad de la Compañía o los proveedores de la Compañía. Tenga en cuenta que estos Términos y el
acceso al Sitio no le otorgan ningún derecho, título o interés sobre ningún derecho de propiedad intelectual, excepto
los derechos de acceso limitado expresados en la Sección 2.1. La Compañía y sus proveedores se reservan todos los
derechos no otorgados en estos Términos.

## Enlaces y anuncios de terceros; Otros usuarios

**Enlaces y anuncios de terceros.** El Sitio puede contener enlaces a sitios web y servicios de terceros, y/o mostrar
anuncios de terceros. Dichos enlaces y anuncios de terceros no están bajo el control de la Compañía, y la Compañía no es
responsable de los enlaces y anuncios de terceros. La Compañía proporciona acceso a estos Vínculos y Anuncios de
Terceros solo para su conveniencia, y no revisa, aprueba, monitorea, respalda, garantiza ni hace ninguna declaración con
respecto a los Vínculos y Anuncios de Terceros. Usted utiliza todos los enlaces y anuncios de terceros bajo su propio
riesgo y debe aplicar un nivel adecuado de precaución y discreción al hacerlo. Cuando hace clic en cualquiera de los
Vínculos y Anuncios de Terceros, se aplican los términos y políticas aplicables del tercero, incluidas las prácticas de
privacidad y recopilación de datos del tercero.

**Otros usuarios.** Cada usuario del Sitio es el único responsable de todos y cada uno de su propio Contenido de
usuario. Debido a que no controlamos el Contenido del usuario, usted reconoce y acepta que no somos responsables de
ningún Contenido del usuario, ya sea proporcionado por usted o por otros. Usted acepta que la Compañía no será
responsable de ninguna pérdida o daño incurrido como resultado de dichas interacciones. Si hay una disputa entre usted y
cualquier usuario del Sitio, no tenemos la obligación de involucrarnos.

Por la presente, usted libera y libera para siempre a la Compañía y a nuestros funcionarios, empleados, agentes,
sucesores y cesionarios de, y por la presente renuncia y renuncia a todas y cada una de las disputas, reclamos,
controversias, demandas, derechos, obligaciones, responsabilidades pasadas, presentes y futuras, acción y causa de
acción de todo tipo y naturaleza, que haya surgido o surja directa o indirectamente de, o que se relacione directa o
indirectamente con el Sitio. Si usted es un residente de California, por la presente renuncia a la sección 1542 del
código civil de California en relación con lo anterior, que establece: "una exención general no se extiende a las
reclamaciones que el acreedor no sabe o sospecha que existen a su favor en el momento de la ejecución de la liberación,
que de ser conocida por él o ella debe haber afectado materialmente su liquidación con el deudor."

**Cookies y balizas web.** Como cualquier otro sitio web, {{< param "company.name" >}} utiliza "cookies". Estas cookies se
utilizan para almacenar información, incluidas las preferencias de los visitantes y las páginas del sitio web a las que
el visitante accedió o visitó. La información se utiliza para optimizar la experiencia de los usuarios al personalizar
el contenido de nuestra página web en función del tipo de navegador de los visitantes y/u otra información.

## Descargos de responsabilidad

El sitio se proporciona "tal cual" y "según esté disponible", y la empresa y nuestros proveedores renuncian expresamente
a todas y cada una de las garantías y condiciones de cualquier tipo, ya sean expresas, implícitas o legales, incluidas
todas las garantías o condiciones de comerciabilidad. , idoneidad para un propósito, título, disfrute silencioso,
precisión o no infracción en particular. Nosotros y nuestros proveedores no garantizamos que el sitio cumpla con sus
requisitos, que esté disponible de manera ininterrumpida, oportuna, segura o sin errores, o que sea preciso, confiable,
libre de virus u otro código dañino, completo, legal, o seguro. Si la ley aplicable requiere alguna garantía con
respecto al sitio, todas estas garantías tienen una duración limitada a noventa (90) días a partir de la fecha del
primer uso.

Algunas jurisdicciones no permiten la exclusión de garantías implícitas, por lo que es posible que la exclusión anterior
no se aplique en su caso. Algunas jurisdicciones no permiten limitaciones sobre la duración de una garantía implícita,
por lo que es posible que la limitación anterior no se aplique en su caso.

## Limitación de responsabilidad

En la máxima medida permitida por la ley, en ningún caso la empresa o nuestros proveedores serán responsables ante usted
o cualquier tercero por cualquier lucro cesante, pérdida de datos, costos de adquisición de productos sustitutos o
cualquier indirecto, consecuencial, ejemplar, incidental, daños especiales o punitivos que surjan de o se relacionen con
estos términos o su uso o incapacidad para usar el sitio, incluso si la empresa ha sido informada de la posibilidad de
tales daños. El acceso y uso del sitio es a su propia discreción y riesgo, y usted será el único responsable de
cualquier daño a su dispositivo o sistema informático, o pérdida de datos resultante de ello.

En la medida máxima permitida por la ley, sin perjuicio de cualquier disposición en contrario contenida en este
documento, nuestra responsabilidad ante usted por cualquier daño que surja de este acuerdo o esté relacionado con él, se
limitará en todo momento a un máximo de cincuenta dólares estadounidenses (50 dólares estadounidenses). La existencia de
más de una reclamación no ampliará este límite. Usted acepta que nuestros proveedores no tendrán responsabilidad de
ningún tipo derivada o relacionada con este acuerdo.

Algunas jurisdicciones no permiten la limitación o exclusión de responsabilidad por daños incidentales o consecuentes,
por lo que es posible que la limitación o exclusión anterior no se aplique en su caso.

**Término y rescisión.** Sujeto a esta Sección, estos Términos permanecerán en pleno vigor y efecto mientras usa el
Sitio. Podemos suspender o rescindir sus derechos de uso del Sitio en cualquier momento y por cualquier motivo a nuestro
exclusivo criterio, incluso para cualquier uso del Sitio en violación de estos Términos. Tras la terminación de sus
derechos en virtud de estos Términos, su Cuenta y el derecho de acceso y uso del Sitio finalizarán de inmediato. Usted
comprende que cualquier cancelación de su Cuenta puede implicar la eliminación de su Contenido de usuario asociado con
su Cuenta de nuestras bases de datos en vivo. La Compañía no tendrá responsabilidad alguna ante usted por la terminación
de sus derechos bajo estos Términos. Incluso después de que se rescindan sus derechos en virtud de estos Términos, las
siguientes disposiciones de estos Términos permanecerán en vigor: Secciones 2 a 2.5, Sección 3 y Secciones 4 a 10.

## Política de derechos de autor

La Compañía respeta la propiedad intelectual de otros y solicita que los usuarios de nuestro Sitio hagan lo mismo. En
relación con nuestro Sitio, hemos adoptado e implementado una política que respeta la ley de derechos de autor que prevé
la eliminación de cualquier material infractor y la terminación de los usuarios de nuestro Sitio en línea que infrinjan
repetidamente los derechos de propiedad intelectual, incluidos los derechos de autor. Si cree que uno de nuestros
usuarios, a través del uso de nuestro Sitio, infringe ilegalmente los derechos de autor de una obra y desea que se
elimine el material presuntamente infractor, la siguiente información en forma de notificación por escrito (de
conformidad con a 17 USC &sect; 512 (c)) debe proporcionarse a nuestro Agente de derechos de autor designado:

- Tu firma física o electrónica;
- identificación de las obras protegidas por derechos de autor que afirma haber sido infringidas;
- identificación del material de nuestros servicios que usted afirma que infringe y que solicita que eliminemos;
- información suficiente que nos permita localizar dicho material;
- su dirección, número de teléfono y dirección de correo electrónico;
- una declaración de que usted cree de buena fe que el uso del material objetable no está autorizado por el propietario
  de los derechos de autor, su agente o por la ley; y
- una declaración de que la información en la notificación es precisa y, bajo pena de perjurio, que usted es el
  propietario de los derechos de autor que supuestamente se han infringido o que está autorizado para actuar en nombre
  del propietario de los derechos de autor.

Tenga en cuenta que, de conformidad con 17 U.S.C. &sect; 512 (f), cualquier tergiversación de un hecho material en una
notificación por escrito automáticamente somete a la parte reclamante a la responsabilidad por los daños, costos y
honorarios de abogados en los que incurramos en relación con la notificación por escrito y el alegato de infracción de
derechos de autor.

## General

Estos Términos están sujetos a revisión ocasional, y si realizamos cambios sustanciales, podemos notificarle enviándole
un correo electrónico a la última dirección de correo electrónico que nos proporcionó y/o publicando un aviso destacado
de los cambios en nuestro Sitio. Usted es responsable de proporcionarnos su dirección de correo electrónico más
actualizada. En el caso de que la última dirección de correo electrónico que nos haya proporcionado no sea válida,
nuestro envío del correo electrónico que contiene dicho aviso constituirá, no obstante, un aviso efectivo de los cambios
descritos en el aviso. Cualquier cambio a estos Términos entrará en vigencia a partir de los treinta (30) días
calendario siguientes a nuestro envío de un aviso por correo electrónico o treinta (30) días calendario después de
nuestra publicación del aviso de los cambios en nuestro Sitio. Estos cambios entrarán en vigencia de inmediato para los
nuevos usuarios de nuestro Sitio. El uso continuado de nuestro Sitio después de la notificación de dichos cambios
indicará su reconocimiento de dichos cambios y su acuerdo de estar sujeto a los términos y condiciones de dichos
cambios.

**Resolución de conflictos.** Lea este Acuerdo de arbitraje detenidamente. Es parte de su contrato con la Compañía
y afecta sus derechos. Contiene procedimientos para ARBITRAJE VINCULANTE OBLIGATORIO Y RENUNCIA A DEMANDAS COLECTIVAS.

**Aplicabilidad del Acuerdo de Arbitraje.** Todas las reclamaciones y disputas en relación con los Términos o el uso de
cualquier producto o servicio proporcionado por la Compañía que no se pueda resolver de manera informal o en un tribunal
de reclamos menores se resolverán mediante arbitraje vinculante de forma individual. bajo los términos de este Acuerdo
de Arbitraje. A menos que se acuerde lo contrario, todos los procedimientos de arbitraje se llevarán a cabo en inglés.
Este Acuerdo de arbitraje se aplica a usted y a la Compañía, y a todas las subsidiarias, afiliadas, agentes, empleados,
predecesores en interés, sucesores y cesionarios, así como a todos los usuarios o beneficiarios autorizados o no
autorizados de los servicios o bienes proporcionados en virtud de los Términos.

**Requisito de notificación y resolución informal de disputas.** Antes de que cualquiera de las partes pueda solicitar
un arbitraje, la parte debe enviar a la otra parte una Notificación de disputa por escrito que describa la naturaleza y
la base de la reclamación o disputa, y la reparación solicitada. Se debe enviar un Aviso a la Compañía a:
_{{< param "company.address" >}}_. Una vez recibido el Aviso, usted y la Compañía pueden intentar resolver el reclamo o
disputa de manera informal. Si usted y la Compañía no resuelven el reclamo o disputa dentro de los treinta (30) días
posteriores a la recepción del Aviso, cualquiera de las partes puede iniciar un procedimiento de arbitraje. El monto de
cualquier oferta de conciliación hecha por cualquiera de las partes no puede ser revelado al árbitro hasta después de
que el árbitro haya determinado el monto del laudo al que cualquiera de las partes tiene derecho.

**Reglas de arbitraje.** El arbitraje se iniciará a través de la Asociación Estadounidense de Arbitraje, un proveedor
alternativo de resolución de disputas establecido que ofrece arbitraje como se establece en esta sección. Si AAA no está
disponible para arbitrar, las partes acordarán seleccionar un Proveedor de ADR alternativo. Las reglas del Proveedor de
ADR regirán todos los aspectos del arbitraje, excepto en la medida en que dichas reglas entren en conflicto con los
Términos. Las Reglas de arbitraje del consumidor de la AAA que rigen el arbitraje están disponibles en línea en adr.org
o llamando a la AAA al 1-800-778-7879. El arbitraje será realizado por un árbitro único y neutral. Cualquier reclamo o
disputa donde el monto total del laudo solicitado sea menor a Diez mil dólares estadounidenses (US $ 10,000.00) puede
resolverse a través de un arbitraje vinculante sin comparecencia, a opción de la parte que busca el alivio. En el caso
de reclamaciones o disputas en las que el monto total de la indemnización solicitada sea de diez mil dólares
estadounidenses (US $ 10.000,00) o más, el derecho a una audiencia será determinado por las Reglas de Arbitraje.
Cualquier audiencia se llevará a cabo en un lugar dentro de las 100 millas de su residencia, a menos que resida fuera de
los Estados Unidos y a menos que las partes acuerden lo contrario. Si reside fuera de los EE. UU., El árbitro notificará
a las partes con razonable antelación la fecha, la hora y el lugar de las audiencias orales. Cualquier juicio sobre el
laudo dictado por el árbitro puede ser presentado en cualquier tribunal de jurisdicción competente. Si el árbitro le
otorga un laudo que es mayor que la última oferta de conciliación que la Compañía le hizo antes del inicio del
arbitraje, la Compañía le pagará el monto mayor entre el laudo o $ 2,500.00. Cada parte asumirá sus propios costos y
desembolsos que surjan del arbitraje y pagará una parte igual de los honorarios y costos del Proveedor de ADR.

**Reglas adicionales para el arbitraje no basado en comparecencia.** Si se elige un arbitraje basado en no
comparecencia, el arbitraje se llevará a cabo por teléfono, en línea y/o basándose únicamente en presentaciones
escritas; la forma específica será elegida por la parte que inicia el arbitraje. El arbitraje no implicará ninguna
comparecencia personal de las partes o testigos a menos que las partes acuerden lo contrario.

**Límites de tiempo.** Si usted o la Compañía buscan un arbitraje, la acción de arbitraje debe iniciarse y/o exigirse
dentro del estatuto de limitaciones y dentro de cualquier plazo impuesto por las Reglas de la AAA para el reclamo
pertinente.

**Autoridad del árbitro.** Si se inicia el arbitraje, el árbitro decidirá los derechos y responsabilidades de usted y la
Compañía, y la disputa no se consolidará con ningún otro asunto ni se unirá a otros casos o partes. El árbitro tendrá la
autoridad para otorgar mociones dispositivas de todo o parte de cualquier reclamo. El árbitro tendrá la autoridad para
otorgar daños monetarios y otorgar cualquier remedio o compensación no monetaria disponible para un individuo bajo la
ley aplicable, las Reglas de la AAA y los Términos. El árbitro emitirá un laudo por escrito y una declaración de
decisión que describa los hallazgos y conclusiones esenciales en los que se basa el laudo. El árbitro tiene la misma
autoridad para otorgar reparaciones de manera individual que tendría un juez en un tribunal de justicia. El laudo del
árbitro es definitivo y vinculante para usted y la Compañía.

**Renuncia al juicio con jurado.** LAS PARTES POR LA PRESENTE RENUNCIAN A SUS DERECHOS CONSTITUCIONALES Y ESTATUTARIOS
DE IR A LA CORTE Y TENER UN JUICIO FRENTE A UN JUEZ O JURADO, en lugar de optar por que todas las reclamaciones y
disputas se resuelvan mediante arbitraje en virtud de este Arbitraje Convenio. Los procedimientos de arbitraje suelen
ser más limitados, más eficientes y menos costosos que las reglas aplicables en un tribunal y están sujetos a una
revisión muy limitada por parte de un tribunal. En caso de que surja algún litigio entre usted y la Compañía en
cualquier tribunal estatal o federal en una demanda para anular o hacer cumplir un laudo arbitral o de otra manera,
USTED Y LA COMPAÑÍA RENUNCIAN A TODOS LOS DERECHOS A UN JUICIO CON JURADO, en lugar de elegir que la disputa se
resuelva. por un juez.

**Renuncia a acciones colectivas o consolidadas.** Todas las reclamaciones y disputas dentro del alcance de este acuerdo
de arbitraje deben ser arbitradas o litigadas de forma individual y no colectiva, y las reclamaciones de más de un
cliente o usuario no pueden arbitrarse. o litigados de forma conjunta o consolidada con los de cualquier otro cliente o
usuario.

**Confidencialidad.** Todos los aspectos del procedimiento de arbitraje serán estrictamente confidenciales. Las partes
acuerdan mantener la confidencialidad a menos que la ley exija lo contrario. Este párrafo no impedirá que una parte
presente a un tribunal de justicia cualquier información necesaria para hacer cumplir este Acuerdo, para hacer cumplir
un laudo arbitral o para buscar una medida cautelar o una reparación equitativa.

**Divisibilidad.** Si una parte o partes de este Acuerdo de Arbitraje se encuentran bajo la ley como inválidas o
inaplicables por un tribunal de jurisdicción competente, entonces dicha parte o partes específicas no tendrán fuerza ni
efecto y serán cortadas y el resto del Acuerdo continuará en pleno vigor y efecto.

**Derecho a renunciar.** La parte contra la que se hace valer el reclamo puede renunciar a cualquiera o todos los
derechos y limitaciones establecidos en este Acuerdo de arbitraje. Dicha renuncia no renunciará ni afectará ninguna otra
parte de este Acuerdo de arbitraje.

**Duración del acuerdo.** Este Acuerdo de arbitraje sobrevivirá a la terminación de su relación con la Compañía.

**Tribunal de reclamos menores.** No obstante lo anterior, usted o la Compañía pueden entablar una acción individual en
el tribunal de reclamos menores.

**Alivio equitativo de emergencia.** De todos modos, cualquiera de las partes puede solicitar un alivio equitativo de
emergencia ante un tribunal estatal o federal para mantener el statu quo pendiente de arbitraje. Una solicitud de
medidas provisionales no se considerará una renuncia a ningún otro derecho u obligación en virtud de este Acuerdo de
arbitraje.

**Reclamaciones no sujetas a arbitraje.** Sin perjuicio de lo anterior, las reclamaciones por difamación, violación de
la Ley de abuso y fraude informático y la infracción o apropiación indebida de la patente, los derechos de autor, la
marca comercial o los secretos comerciales de la otra parte no estarán sujetos a este Arbitraje. Convenio.

En cualquier circunstancia en la que el Acuerdo de arbitraje anterior permita a las partes litigar en los tribunales,
las partes acuerdan someterse a la jurisdicción personal de los tribunales ubicados dentro del condado de Netherlands,
California, para tales fines.

El Sitio puede estar sujeto a las leyes de control de exportaciones de EE. UU. Y puede estar sujeto a regulaciones de
exportación o importación en otros países. Usted acepta no exportar, reexportar o transferir, directa o indirectamente,
ningún dato técnico de EE. UU. Adquirido de la Compañía, o cualquier producto que utilice dichos datos, en violación de
las leyes o regulaciones de exportación de los EE. UU.

La empresa está ubicada en la dirección que figura en la Sección 10.8. Si es residente de California, puede reportar
quejas a la Unidad de Asistencia para Quejas de la División de Productos del Consumidor del Departamento de Asuntos del
Consumidor de California comunicándose con ellos por escrito en _400 R Street, Sacramento, CA 95814_, o por teléfono
al _(800) 952-5210_.

**Comunicaciones electrónicas.** Las comunicaciones entre usted y la Compañía utilizan medios electrónicos, ya sea que
use el Sitio o nos envíe correos electrónicos, o si la Compañía publica avisos en el Sitio o se comunica con usted por
correo electrónico. Para fines contractuales, usted (a) da su consentimiento para recibir comunicaciones de la Compañía
en forma electrónica; y (b) aceptar que todos los términos y condiciones, acuerdos, avisos, divulgaciones y otras
comunicaciones que la Compañía le proporcione electrónicamente satisfacen cualquier obligación legal que dichas
comunicaciones cumplirían si estuvieran en una copia impresa.

**Términos completos.** Estos Términos constituyen el acuerdo completo entre usted y nosotros con respecto al uso del
Sitio. El hecho de que no ejerzamos o hagamos cumplir cualquier derecho o disposición de estos Términos no funcionará
como una renuncia a dicho derecho o disposición. Los títulos de las secciones en estos Términos son solo para
conveniencia y no tienen ningún efecto legal o contractual. La palabra "incluido" significa "incluido sin limitación".
Si alguna disposición de estos Términos se considera inválida o inaplicable, las demás disposiciones de estos Términos
no se verán afectadas y la disposición inválida o inaplicable se considerará modificada para que sea válida y ejecutable
en la máxima medida permitida por la ley. Su relación con la Compañía es la de un contratista independiente y ninguna de
las partes es agente o socio de la otra. Estos Términos, y sus derechos y obligaciones en el presente, no pueden ser
asignados, subcontratados, delegados o transferidos por usted sin el consentimiento previo por escrito de la Compañía, y
cualquier intento de asignación, subcontrato, delegación o transferencia en violación de lo anterior será nulo y vacío.
La Compañía puede asignar libremente estos Términos. Los términos y condiciones establecidos en estos Términos serán
vinculantes para los cesionarios.

**Su privacidad.** Lea nuestra Política de privacidad.

**Información de copyright/marca comercial.** Copyright &copy;. Reservados todos los derechos. Todas las marcas
comerciales, logotipos y marcas de servicio que se muestran en el Sitio son de nuestra propiedad o de terceros. No se le
permite utilizar estas Marcas sin nuestro consentimiento previo por escrito o el consentimiento de dicho tercero que
pueda ser propietario de las Marcas.

## Información del contacto

Si tiene alguna pregunta sobre estos Términos y condiciones, puede [contactarnos](/es/contact/).

Versión 1.0
