+++
title = "Envíenos una nota"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "contact"
draft = false

[menu.main]
name = "Contactar"
weight = 70

[menu.footer]
name = "Contactar"
weight = 70


+++

Estamos convenientemente ubicados cerca de las autopistas 57 y 60 en Diamond Bar, California. Gire hacia el camino
de entrada adyacente al edificio IBEW Local 47. Nuestra oficina está en el mismo complejo.

Desde las personas que le dan la bienvenida a nuestra oficina hasta las que trabajan junto a nuestros médicos,
siempre encontrará que nuestro personal es profesional y atento. Somos una familia y esperamos que se una a nosotros.

Las citas y los recordatorios se pueden hacer por correo electrónico y por teléfono. Llame a nuestra oficina para
cualquier situación de emergencia para que podamos verlo lo antes posible.
