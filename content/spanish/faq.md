+++
title = "Preguntas frecuentes"
description = ""
subtitle = "Aquí hay algunas preguntas que nos hacen con frecuencia."
lastmod = "2021-09-09T00:00:00"
bg_image = "images/bg/page-title.jpg"
layout = "faq"
draft = false

[menu.main]
name = "FAQ"
weight = 80

[menu.footer]
name = "FAQ"
weight = 80

+++

### 1. ¿Por qué mis dientes son sensibles?

Los dientes sensibles a menudo provienen del hecho de que sus encías han retrocedido levemente. Esta recesión de la
línea de las encías permite que se vea la dentina subyacente, lo que permite que el agua y los alimentos accedan más
fácilmente al nervio sensible. Para manejar esto, hay una serie de pastas dentales, geles e incluso algunos
procedimientos dentales que se pueden aplicar. Háblenos con más detalle si tiene dientes muy sensibles.

### 2. ¿Qué debo hacer para prevenir la enfermedad de las encías y la caries dental?

El cuidado excelente de los dientes y las encías comienza en casa. Cepillarse los dientes y usar hilo dental a diario es
la mejor manera de cuidar sus dientes y encías de manera continua. Al mantener una rutina diaria, minimizará en gran
medida el riesgo de gingivitis o caries dentales a medida que envejece.

### 3. ¿Qué es la gingivitis?

_La gingivitis_ es una afección causada cuando las bacterias rodean los dientes y penetran en las encías. Las encías pueden irritarse, inflamarse, y, frecuentemente, sangrar. Para evitar que la afección empeore, se recomienda encarecidamente realizar visitas periódicas de higiene. Durante su visita, nuestro equipo de higiene le enseñará las técnicas adecuadas de uso del hilo dental y el protocolo de higiene bucal para la atención domiciliaria evitará la enfermedad periodontal.

### 4. ¿Qué es la enfermedad periodontal?

_La enfermedad periodontal_ es una enfermedad silenciosa que comienza con pocos o ningún síntoma. Es causada por bacterias que rodean los dientes y penetran en las encías. La afección inmediata se conoce como "gingivitis". Las encías se irritan, se inflaman y sangran con frecuencia. Si no se trata adecuadamente, la afección empeora. Ahora aparecen síntomas notables. Incluyen:

- Mal aliento
- Recesión de las encías
- Sensibilidad de las encías a los alimentos ácidos
- Absceso de las encías
- Dolor de muelas
- Pérdida de dientes

### 5. ¿Cómo se trata la enfermedad periodontal?

_La enfermedad periodontal_ es una afección crónica que requiere atención inmediata. Mediante una serie de limpiezas periodontales, alisado y raspado radicular, terapia con láser y antibióticos locales, esta afección se puede controlar. La cirugía periodontal solo es necesaria para casos graves.

### 6. ¿Cuál es la diferencia entre un relleno blanco y un relleno plateado?

_Los empastes de plata_ conocidos como amalgama existen desde hace décadas. Hecho de una aleación de metal, fue la mejor restauración para empastes. El metal se expande y contrae con el calor y el frío que se colocan en la boca. Esto permitió que pequeñas bacterias ingresaran al diente una vez que se obturaba; mantener el diente sano y fuerte.

_Los empastes blancos_, también conocidos como compuestos, a menudo están hechos de polímeros de plástico o vidrio. Estos empastes cosméticos nos permiten llenar una cavidad con una sustancia que se verá y se sentirá como su estructura dental existente. Esta restauración se crea con un material de resina y se ajusta perfectamente a un diente para prevenir la caries. En lugar de un material gris o plateado en la boca, el color compuesto coincidirá con el color del diente.

### 7. ¿Cómo puedo mejorar mi sonrisa?

Hay varias formas en el mundo dental actual de mejorar su sonrisa. Algunos procedimientos incluyen:

- Blanqueamiento dental
- Vinculación
- Carillas de porcelana
- Coronas de porcelana

Tenemos la capacidad de mejorar su sonrisa mediante todos o algunos de estos procedimientos. Para una consulta exacta, comuníquese con nuestra oficina para que podamos brindarle un plan de tratamiento personalizado.

### 8. ¿Qué es el blanqueamiento dental?

_El blanqueamiento dental_ es un procedimiento rentable y seguro para crear una sonrisa hermosa y saludable. A lo largo de los años, se ha agregado fluoruro al producto blanqueador. Esto reduce el riesgo de sensibilidad en dientes y encías. El blanqueamiento dental debe ser controlado por su dentista y solo debe realizarse después de un examen completo y una limpieza higiénica.

El proceso de blanqueamiento puede durar varios años si se mantiene correctamente. Las bebidas como café, té, cola y vino reducirán el efecto duradero. Recuerde, si puede manchar una camisa blanca, ¡manchará su sonrisa!

### 9. ¿Qué es la vinculación?

_La vinculación_ es un procedimiento rentable que se utiliza para llenar los espacios en los dientes frontales y para cambiar el color de un diente. Los resultados inmediatos son increíbles. ¡En unas pocas horas, tendrás una gran sonrisa! La vinculación como el blanqueamiento dental puede cambiar de color con el tiempo debido al café, té, cola y vino.

### 10. ¿Qué son las carillas de porcelana?

_Las carillas de porcelana_ son piezas delgadas de porcelana que van directamente sobre los dientes naturales. Todo este procedimiento puede llevar tan solo dos visitas. Las carillas cambian el tamaño, la forma y el color de los dientes de un paciente. Este procedimiento se utiliza para reparar dientes fracturados, dientes oscurecidos por la edad o medicamentos, o una sonrisa torcida. Muchas veces, los pacientes piden carillas de porcelana para sentirse y verse más jóvenes con una sonrisa más recta y blanca.

### 11. ¿Qué son las coronas?

_Las coronas_ son un procedimiento cosmético permanente que cubre todo el diente. Cambiará el tamaño, la forma y el color de los dientes en tan solo 2 visitas.

### 12. ¿Qué es un implante dental?

_Un implante dental_ es un reemplazo "hecho por el hombre" para un diente faltante o la raíz del diente. Hecho de titanio, este objeto en forma de tornillo se inserta debajo de la encía y directamente en el hueso de la mandíbula superior o inferior. Usualmente hay una minima incomodidad involucrada con este procedimiento. Después de unos meses, el implante dental y el hueso se fusionan. Esto crea un ancla para que el nuevo diente se coloque sobre el implante dental.

### 13. Cuales son los beneficios de los implantes dentales?

- Los implantes dentales se ven y funcionan como su diente natural.
- Los implantes dentales son una solución permanente para los dientes perdidos.
- Los implantes dentales se mantienen mediante visitas de higiene de rutina a su consultorio dental.
- Los implantes dentales reducen la posibilidad de pérdida ósea, enfermedad periodontal, movimiento de los dientes y una mayor pérdida de dientes.
- Los implantes dentales reemplazan la necesidad de una prótesis total o parcial removible.
- Los implantes dentales se enfocan solo en el diente o dientes que faltan. Un puente tradicional implicaría que los dos o más dientes adyacentes se vean comprometidos para crear un diente postizo en el medio.

### 14. ¿Quién es candidato para implantes dentales?

Con importantes avances en odontología e implantes dentales, la mayoría de las personas son candidatas a implantes dentales. Puede haber excepciones debido a enfermedades crónicas, cardiopatías y osteoporosis grave.

### 15. ¿Qué implica el procedimiento de implante dental?

El procedimiento de implante dental promedio toma de 3 a 4 visitas. La primera visita consiste en realizar una radiografía de la zona y tomar una impresión para una guía quirúrgica y una prótesis temporal para cubrir el implante.

La próxima visita es para colocar el implante. Se aplica anestesia local en el área. (Cualquier sedación adicional ya no es necesaria a menos que lo considere el dentista). Luego, el dentista hará una pequeña incisión para colocar el implante. El implante se coloca en el hueso de la mandíbula. Luego, el área se cubrirá con suturas. El procedimiento generalmente se completa con un dolor leve.

Regresará en aproximadamente 3 meses para comenzar a crear la corona de porcelana para colocarla sobre el implante.

### 16. ¿Cuánto cuesta un implante dental?

Las tarifas de los implantes dentales varían de un dentista a otro. Siempre programe una consulta de implantes para discutir el procedimiento y todos los costos involucrados.

### 17. ¿Cuánto dura un implante dental?

Con una higiene dental de rutina programada y una atención domiciliaria adecuada, un implante dental puede durar aproximadamente de 30 años a toda la vida.

### 18. ¿Su oficina ofrece financiamiento por los servicios prestados?

¡[Comuníquese](/es/contact/) con nosotros para discutir las opciones que tenemos disponibles para hacer su sonrisa perfecta hoy!
