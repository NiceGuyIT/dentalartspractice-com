+++
title = "Acerca de Práctica de Artes Dentales"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "about"
draft = false

#[menu.main]
#name = "Sobre"
#weight = 50

#[menu.footer]
#name = "Sobre"
#weight = 50

[about]
enable = true
image = "images/company/about.jpg"
title = ""
content = """
"""

[about.button]
enable = false
label = ""
link = "#"

[features]
enable = false
title = ""

[[features.feature_item]]
icon = "ion-ios-color-filter-outline"
name = "IOS App Development"
content = ""

[[features.feature_item]]
icon = "ion-ios-unlocked-outline"
name = "App Secutity"
content = ""

[[features.feature_item]]
icon = "ion-ios-game-controller-b-outline"
name = "Games Development"
content = ""

[[features.feature_item]]
icon = "ion-ios-mic-outline"
name = "Animation and Editing"
content = ""

[[features.feature_item]]
icon = "ion-ios-lightbulb-outline"
name = "UI/UX Design"
content = ""

[[features.feature_item]]
icon = "ion-ios-star-outline"
name = "Branding"
content = ""

[testimonial]
enable = false

[mission_vision]
enable = false

[[mission_vision.tabs]]
name = "Vision"
content = """
"""

[[mission_vision.tabs]]
name = "Mission"
content = """
"""

[[mission_vision.tabs]]
name = "Approach"
content = """
"""

[cta]
enable = true

+++
