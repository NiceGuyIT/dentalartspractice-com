+++
[menu.main]
name = "Hogar"
weight = 10

[banner]
enable = true
bg_image = "images/bg/slider-bg-01.jpg"
bg_overlay = true
title = "Odontología para los conscientes de la calidad"
content = ""

[banner.button]
enable = false
#label = "Discover Our Project"
#link = "project"

[about]
enable = true
title = ""
description = """
"""
content = """
Desde el momento en que ingresa a la práctica dental del Dr. Nazari, sabrá que se encuentra en una práctica
extraordinaria. Somos una práctica de múltiples especialidades y utilizamos la última tecnología dental para tratar a
los pacientes con la habilidad y la pericia que provienen de años de experiencia.

Nuestro prostodoncista y periodoncista son expertos en sus respectivos campos de la odontología y trabajan en estrecha
colaboración con nuestros dentistas generales para garantizar que reciba la mejor atención posible. Elegir a su dentista
es algo difícil de hacer, así que venga y le daremos un recorrido por la oficina y le presentaremos a nuestro personal.
"""

image = "images/bg/side-image-01.jpg"

[portfolio]
enable = true
bg_image = "images/bg/slider-bg-02.jpg"
title = "¡Sonrisa! Estás en GRANDES manos."
content = """
Nos gustaría darle la bienvenida a nuestra oficina. Nos complace ayudarlo a mantener una salud bucal óptima. Nuestra
práctica está dedicada a la atención integral y preventiva del paciente.

A lo largo de nuestro sitio web, encontrará abundante información sobre nuestra práctica, los procedimientos que
ofrecemos y la odontología en general. Explore y aprenda todo lo que desee sobre odontología y nuestros servicios.
Creemos que nuestros pacientes deben tener tanta información como sea posible para poder tomar decisiones importantes e
informadas con respecto a su salud bucal y sus opciones de tratamiento.

Nuestros pacientes son nuestro activo más importante y nos esforzamos por desarrollar relaciones duraderas y de
confianza con todos nuestros pacientes. Sus referencias son bienvenidas y apreciadas.
"""

[portfolio.button]
enable = false
label = "Ver obras"
link = "project"

[service]
enable = true
# This one doesn't work.
title = "Nuestros servicios"

[cta]
enable = false
bg_image = "images/call-to-action-bg.jpg"
title = ""
content = """
"""

[cta.button]
enable = true
label = "Tell Us Your Story"
link = "contact"

[funfacts]
enable = false
title = ""
description = "''"

[[funfacts.funfact_item]]
icon = "ion-ios-chatboxes-outline"
name = "Cups Of Coffee"
count = "99"

[[funfacts.funfact_item]]
icon = "ion-ios-glasses-outline"
name = "Article Written"
count = "45"

[[funfacts.funfact_item]]
icon = "ion-ios-compose-outline"
name = "Projects Completed"
count = "125"

[[funfacts.funfact_item]]
icon = "ion-ios-timer-outline"
name = "Combined Projects"
count = "200"

[[funfacts.testimonial_slider]]
name = "Raymond Roy"
image = "images/clients/avater-1.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

[[funfacts.testimonial_slider]]
name = "Randi Renin"
image = "images/clients/avater-1.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

[[funfacts.testimonial_slider]]
name = "Rose Rio"
image = "images/clients/avater-3.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

+++
