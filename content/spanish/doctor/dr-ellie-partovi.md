+++
title = "Dr. Ellie Partovi"
# Shown below the title in the header
description = "**Periodoncista**, DDS, MS"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-ellie-partovi-1200x900.jpg"

# Weight for sorting
weight = 30

# type
type = "doctor"

+++

La Dra. Partovi se graduó de UCLA en 1999. Completó su residencia periodontal en la Universidad de St. Louis en 2006. La Dra. Partovi está muy orientada a la familia y le encanta pasar tiempo con su familia. En su tiempo libre le gusta probar nuevos restaurantes y le encanta viajar.

A Ellie Partovi le encanta entablar relaciones con sus pacientes, lo que le permite conectarse y hacer que los pacientes se sientan mejor en la silla. Como profesional dental, su misión es utilizar la integridad y la inspiración para brindar el mejor cuidado dental posible. Su objetivo es mejorar la vida de las personas y darles la oportunidad de sonreír con confianza y lograr la salud del paciente.

