+++
title = "Dr. Rachel Lim"
# Shown below the title in the header
description = "**Endodoncista**, DMD"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-rachel-lim-703x800.jpg"

# Weight for sorting
weight = 40

# type
type = "doctor"

+++

Hola, mi nombre es Dra. Rachel Lim. Recibí mi entrenamiento especializado y certificado en Endodoncia de la Universidad
de Columbia. Antes de mi capacitación especializada, completé mi residencia de Educación Avanzada en Odontología General
de la Universidad del Pacífico y practiqué como dentista general durante cinco años en Los Ángeles. Me gradué de la
facultad de odontología con honores de la Western University of Health Sciences y fui incluido en la sociedad de honor
Omicron Kappa Upsilon. El Dr. Lim creció en el condado de Orange y disfruta explorando nuevas ciudades, haciendo
caminatas y pasando tiempo de calidad con sus seres queridos.
