+++
title = "Dr. Janet Choi"
# Shown below the title in the header
description = "**Dentista general**, DDS, MS"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-janet-choi-847x847.jpg"

# Weight for sorting
weight = 20

# type
type = "doctor"

+++

La Dra. Janet Choi obtuvo su Licenciatura en Ciencias en la Universidad de California en Berkeley y su Doctorado en Cirugía Dental en la Universidad
del Pacífico en San Francisco. Realizó su residencia en Educación Avanzada en Odontología General en Phoenix, Arizona, donde
trabajó en estrecha colaboración con la población desfavorecida en una organización sin fines de lucro.

La Dra. Choi se dedica a personalizar la atención para cada uno de sus pacientes para lograr sus objetivos de una sonrisa más saludable.
Constantemente realiza cursos de Educación Continua para mantenerse al día con los últimos avances.

Fuera del trabajo, a la Dra. Choi le gusta mantenerse activa, ya sea haciendo caminatas o pilates, así como explorando nuevos
restaurantes.