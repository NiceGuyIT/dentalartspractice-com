+++
title = "Dr. Maxwell M. Nazari"
# Shown below the title in the header
description = "**Prostodoncista y Fellow en Cirugía de Implantes**, DDS, MS"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-maxwell-nazari-900x1200.jpg"

# Weight for sorting
weight = 10

# type
type = "doctor"

+++

El Dr. Nazari ha estado practicando odontología desde 1991. En 2005 completó un programa de residencia de tres años en el prestigioso Programa de Prostodoncia de la Universidad de Loma Linda. El Dr. Nazari también completó el programa LLU Surgery Fellowship donde recibió cirugía de implantes, injertos óseos y manejo de tejidos blandos. Además, el Dr. Nazari tiene una Maestría en Ciencias de la Facultad de Odontología de LLU.

El Dr. Nazari cree que la filosofía de "Odontología para los que se preocupan por la calidad" implica la construcción de relaciones de confianza, la comprensión de las preocupaciones y los deseos de los pacientes, así como la prestación de una atención clínica excepcional. En su tiempo libre, al Dr. Nazari le encanta hacer ejercicio, viajar y pasar tiempo de calidad con su familia.
