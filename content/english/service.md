+++
title = "Services"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "service"
draft = false

[menu.main]
name = "Services"
weight = 20

[menu.footer]
name = "Services"
weight = 20

############################## About Service ##############################
[about]
enable = false
title = ""
content = """
"""
image = "images/company/company-group-pic.jpg"

# Should be no more than 3 features services.
[featured_service]
enable = false

############################## Featured Service ###########################
[[featured_service.service_item]]
name = "Featured Service 1"
icon = "magic"
color = "primary"
content = ""

[[featured_service.service_item]]
name = "Featured Service 2"
icon = "image"
color = "primary-dark"
content = ""

[[featured_service.service_item]]
name = "Featured Service 3"
icon = "leaf"
color = "primary-darker"
content = ""


############################## Service ####################################
[service]
enable = true
title = "Our Services"
description = ""

[[service.service_item]]
icon = "magic"
icon_color = "#0000cd"
category = "Dental Implants"
name = "Dental Implants"
content = """
Dental implants are a permanent and appealing solution to replace missing or extracted teeth.
"""

[[service.service_item]]
icon = "teeth"
icon_color = "#ad76ff"
category = "Dental Hygiene"
name = "Dental Hygiene"
content = """
While at our office, we make sure that you receive the highest level of service and ensure that our dental work is of
the highest quality.
"""

[[service.service_item]]
icon = "image"
icon_color = "#ffd700"
name = "Cosmetic"
category = "Veneers"
content = """
Veneers, Whitening, Bonding and white fillings, Inlays and Onlays
"""

[[service.service_item]]
icon = "cubes"
icon_color = "#800080"
name = "Restorative"
category = "Bridges"
content = """
Bridges, Crowns, Dentures, Root Canals
"""

[[service.service_item]]
icon = "tooth"
icon_color = "#87cefa"
name = "Periodontic"
category = "Arestin"
content = """
Laser, Biopsy, Bone Grafting, Ridge Augmentation, Sinus Augmentation, Canine Exposure, Crown Lengthening, Frenectomy,
Gingival / Gum Grafting
"""

[[service.service_item]]
icon = "mortar-pestle"
icon_color = "#778899"
name = "Orthodontic"
category = "Clear Correct"
content = """
ClearCorrect, Sparks, Clear Aligners, Night Guards, TMJ
"""

[[service.service_item]]
icon = "stethoscope"
icon_color = "#7fe1a6"
name = "Endodontist"
category = ""
content = """
Root canal Anterior, Bicuspid and Molars, Retreatment of Root Canals, Apicoectomies, Pulpal Debridement (Open & Med)
"""


############################## Call to Action #############################
[cta]
enable = true

+++
