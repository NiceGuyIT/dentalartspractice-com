+++
title = "Frequently Asked Questions"
description = ""
subtitle = "Here are some questions that we get asked frequently."
lastmod = "2021-09-09T00:00:00"
bg_image = "images/bg/page-title.jpg"
layout = "faq"
draft = false

[menu.main]
name = "FAQ"
weight = 80

[menu.footer]
name = "FAQ"
weight = 80

+++

### 1. Why are my teeth sensitive?

Sensitive teeth often come from the fact that your gums have slightly receded. This recession of the gum line allows the underlying dentin to show through which allows water and food easier access to the sensitive nerve. To manage this, there are a number of toothpastes, gels and even some dental procedures that can be applied. Speak to us in more detail if you have very sensitive teeth.

### 2. What should I do to prevent gum disease and tooth decay?

Great teeth and gum care start at home. Brushing and flossing on a daily basis is the best way to take care of your teeth and gums on a continual basis. By keeping to a daily routine you will greatly minimize the risk of gingivitis or tooth decay as you age.

### 3. What is gingivitis?

_Gingivitis_ is a condition caused when bacteria surround the teeth and enters the gums. The gums can become irritated,  inflamed and often bleed. In order to prevent the condition from worsening, regular hygiene visits are highly recommended. During your visit, our hygiene team will teach you the proper flossing techniques and oral hygiene protocol for home care will prevent the periodontal disease.

### 4. What is periodontal disease?

_Periodontal Disease_ is a quiet disease that begins with little or no symptoms. It is caused by bacteria that surrounds the teeth and enters the gums. The immediate condition is known as ‘gingivitis’. The gums become irritated, inflamed and often bleed. If not properly treated, the condition worsens. Noticeable symptoms now appear. They include:

 - Bad Breath
 - Gum Recession
 - Gum Sensitivity to Acidic Foods
 - Gum Abscess
 - Tooth Pain
 - Tooth Loss

### 5. How do you treat periodontal disease?

_Periodontal Disease_ is a chronic condition that needs immediate attention. Through a series of periodontal cleanings, root planing & scaling, laser therapy and local antibiotics, this condition can be controlled. Periodontal surgery is only necessary for severe cases.

### 6. What is the difference between a white filling and a silver filling?

_Silver Fillings_ known as amalgam have been around for decades. Made from a metal alloy, it was the best restoration for fillings. The metal expands and contracts with the heat and cold placed in the mouth. This allowed for little bacteria to enter a tooth once filled; keeping the tooth healthy and strong.

_White Fillings_, also known as composites are often made of plastic or glass polymers. These cosmetic fillings allow us to fill a cavity with a substance that will look and feel just like your existing tooth structure. This restoration is created with a resin material and fits tightly into a tooth to prevent decay. Rather than a gray or silver material in your mouth, the composite color will match the tooth color.

### 7. How can I improve my smile?

There are several ways in today’s dental world to enhance your smile. Certain procedures include:

- Teeth Whitening
- Bonding
- Porcelain Veneers
- Porcelain Crowns

We have the capability to improve your smile using all or some of these procedures. For an exact consultation, please contact our office so that we may provide you with a customized treatment plan.

### 8. What is teeth whitening?

_Teeth Whitening_ is a cost-effective and safe procedure to create a beautiful, healthy smile. Over the years, fluoride has been added to the whitening product. This reduces the risk of tooth and gum sensitivity. Teeth whitening must be monitored by your dentist and only done after a comprehensive exam and hygiene cleaning.

The whitening process can last for a number of years if maintained properly. Beverages such as coffee, tea, cola and wine will reduce the lasting effect. Remember, if it could stain a white shirt, it will stain your smile!

### 9. What is bonding?

_Bonding_ is a cost-effective procedure used to fill gaps in front teeth and to change a tooth’s color. The immediate results are amazing. Within a few hours, you will have a great smile! Bonding like teeth whitening may change color over time due to coffee, tea, cola and wine.

### 10. What are porcelain veneers?

_Porcelain Veneers_ are thin pieces of porcelain that go directly on your natural teeth. This entire procedure can take as few as two visits. Veneers change the size, shape and color of a patient’s teeth. This procedure is used to repair fractured teeth, teeth darkened by age or medication, or a crooked smile. Many times, patients ask for porcelain veneers to simply feel and look younger with a straighter, whiter smile!

### 11. What are crowns?

_Crowns_ are a permanent cosmetic procedure that covers the entire tooth. It will change the size, shape and color of the teeth in as few as 2 visits.

### 12. What is a dental implant?

A _Dental Implant_ is a “man-made” replacement for a missing tooth or tooth root. Made from titanium, this screw-like object is inserted under the gum and directly into the upper or lower jaw bone. There is usually minimal discomfort involved with this procedure. After a period of a few months, the dental implant and the bone fuse together. This creates an anchor for the new tooth to be placed onto the dental implant.

### 13. What are the benefits of dental implants?

-   Dental implants look and function like your natural tooth.
-   Dental implants are a permanent solution for missing teeth.
-   Dental implants are maintained by routine hygiene visits to your dental office.
-   Dental implants decrease the possibility of bone loss, periodontal disease, tooth movement, and further tooth loss.
-   Dental implants replace the need for a removable full or partial denture.
-   Dental implants focus only on the tooth or teeth that are missing. A traditional bridge would involve the two or more adjacent teeth being compromised to create a false tooth in between.

### 14. Who is a candidate for dental implants?

With major advancements in dentistry and dental implants, most people are candidates for dental implants. There may be  exceptions due to chronic illness, heart disease, and severe osteoporosis.

### 15. What does the dental implant procedure involve?

The average dental implant procedure takes 3 -- 4 visits. The first visit is to x-ray the area and take an impression for a surgical guide and a temporary prosthesis to cover the implant.

The next visit is to place the implant. A local anesthesia is applied to the area. (Any additional sedation is no longer necessary unless deemed by the dentist). The dentist will then make a minor incision to place the implant. The implant is placed into the jaw bone. The area will then be covered with sutures. The procedure is usually completed with minor pain.

You will return in approximately 3 months to begin creating the porcelain crown to place over the implant.

### 16. How much does a dental implant cost?

Fees from dental implants vary from dentist to dentist. Always schedule an implant consultation to discuss the procedure and all fees involved.

### 17. How long does a dental implant last?

With routine dental hygiene scheduled and proper home care, a dental implant can last approximately 30 years to a lifetime.

### 18. Does your office offer financing for services provided?

Please [contact](/contact/) us to discuss the options we have available to make your perfect smile today!
