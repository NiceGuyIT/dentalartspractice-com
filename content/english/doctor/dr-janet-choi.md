+++
title = "Dr. Janet Choi"
# Shown below the title in the header
description = "**General Dentist**, DDS, MS"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-janet-choi-847x847.jpg"

# Weight for sorting
weight = 20

# type
type = "doctor"

+++

Dr. Janet Choi earned her Bachelor of Science at UC Berkeley and received her Doctor of Dental Surgery at University
of the Pacific in San Francisco. She pursued Advanced Education of General Dentistry residency in Phoenix, AZ, where
she worked closely with the underserved population at a nonprofit organization.

Dr. Choi is dedicated to personalizing care to each of her patients in achieving their goals of a healthier smile.
She consistently pursues Continuing Education courses in order to keep pace with the latest advancements. 

Outside of work, Dr. Choi enjoys staying active, whether going on hikes or doing pilates, as well as exploring new
restaurants.