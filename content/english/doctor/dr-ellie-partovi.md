+++
title = "Dr. Ellie Partovi"
# Shown below the title in the header
description = "**Periodontist**, DDS, MS"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-ellie-partovi-1200x900.jpg"

# Weight for sorting
weight = 30

# type
type = "doctor"

+++

Doctor Partovi graduated from UCLA in 1999. She completed her periodontal residency at St. Louis University in 2006. Dr.
Partovi is very family oriented and loves spending time with her family. In her spare time she likes trying new
restaurants and loves to travel.

Ellie Partovi loves building relationships with her patients which allow her to connect and make patients feel better in
the chair. As a dental professional, her mission is to use integrity and inspiration to deliver the best dental care
possible. Her goal is to enhance people’s lives and give them the opportunity to smile with confidence and accomplish
patient health.
