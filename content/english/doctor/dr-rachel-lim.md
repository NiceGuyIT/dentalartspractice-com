+++
title = "Dr. Rachel Lim"
# Shown below the title in the header
description = "**Endodontist**, DMD"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-rachel-lim-703x800.jpg"

# Weight for sorting
weight = 40

# type
type = "doctor"

+++

Hello, my name is Dr. Rachel Lim. I received my specialty training and certificate in Endodontics from Columbia
University. Prior to my specialized training, I completed my Advanced Education in General Dentistry residency from the
University of the Pacific, and practiced as a general dentist for five years in Los Angeles. I graduated from dental
school with honors from Western University of Health Sciences and was inducted into Omicron Kappa Upsilon honor society.
Dr. Lim grew up in Orange County and enjoys exploring new cities, hiking, and spending quality time with loved ones.
