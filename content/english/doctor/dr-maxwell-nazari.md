+++
title = "Dr. Maxwell M. Nazari"
# Shown below the title in the header
description = "**Prosthodontist and Fellow in Implant Surgery**, DDS, MS"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-maxwell-nazari-900x1200.jpg"

# Weight for sorting
weight = 10

# type
type = "doctor"

+++

Dr. Nazari has been practicing dentistry since 1991. In 2005, he completed a three-year residency program at the
prestigious Loma Linda University Prosthodontics Program. Dr. Nazari also completed the LLU Surgery Fellowship program
where he received implant surgery, bone grafting and soft tissue management. In addition, Dr. Nazari has a Master’s
degree in Science from LLU school of dentistry.

Dr. Nazari believes that the philosophy of “Dentistry for the quality conscious” involves building trusting
relationships, understanding patients’ concerns and wishes, as well as providing exceptional clinical care. In his free
time, Dr. Nazari loves to exercise, travel and spend quality time with his family.
