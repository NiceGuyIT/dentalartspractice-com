+++
title = "Meet Our Team"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "staff"
draft = false

[menu.main]
name = "Staff"
weight = 40

[menu.footer]
name = "Staff"
weight = 40


[staff]
enable = true

# (Order of bios: ME, Marie , Nadia, Monica, Ben, jose, wendy , jonathan )

[[staff.office]]
image = "images/staff/joann-garcia-office-manager-800x800.jpg"
weight = 10
name = "JoAnn Garcia"
title = "Office Manager"
content = """
Hello, my name is JoAnn. I am the Office Manager here at Dental Arts Practice. I have been working in the Dental FieId
for 26 years. When I began working with patients, I took on the position as a Dental Receptionist, where I progressed
into billing, and later took on the responsibility of being an Office Manager. Aside from managing a business, I enjoy
hiking and weightlifting on my free time. Occasionally on the weekends, I also enjoy going to concerts with brothers and
listening to Rock and Roll music. Most importantly, my favorite time of the day is the time I spend with my fiancé, my
two sons, and my dog (FIF).
"""

[[staff.office]]
image = "images/staff/marie-barton-treatment-coordinator-800x800.jpg"
weight = 20
name = "Marie Barton"
title = "Treatment Coordinator"
content = """
Hello, my name is Marie, I grew up in Los Angeles, CA. I began working in the dental field in 1988.l gained my
experience working alongside a doctor, as a dental assistant. When I am away from my friends at the office; I enjoy
spending time with my husband, 4 children, and 2 grandchildren. On the weekends, I enjoy going to football games and
relaxing under the warm sun.
"""

[[staff.office]]
image = "images/staff/nadia-stopani-insurance-biller-800x800.jpg"
weight = 30
name = "Nadia Stopani"
title = "Insurance Biller"
content = """
Hi, my name is Nadia, l was raised in East Los Angeles. l have been working in the dental field for 7 years and have
enjoyed working and making morning coffee for Dr. Nazari. On my spare time when l’m not billing, I love making my
coworkers laugh and bringing life to the quiet office. Lastly, my pride and joys are my 3 children, my 2 grandchildren,
and my 3rd grandchild on the way. GO DODGERS!!!
"""

[[staff.office]]
image = "images/staff/monica-paredes-dental-assistant-800x800.jpg"
weight = 40
name = "Monica Paredes"
title = "Dental Assistant"
content = """
Hi, my name is Monica Paredes. I am a Dental Assistant and have been working in the Dental Field for 26 years. I love
and enjoy helping Dr. Nazari with all the patients that visit us here at Dental Arts Practice. When I am not working, I
love to spend time with my son and 2 grandchildren, and the rest of my family. My favorite hobby is taking my
grandchildren to the beach and laying under the sun!
"""

[[staff.office]]
image = "images/staff/benjamin-ceja-dental-assistant-800x800.jpg"
weight = 50
name = "Benjamin Ceja"
title = "Dental Assistant"
content = """
Hi my name is Benjamin, but they call me biscuit for short. I was raised in Oxnard, and I am new to the dental field. I
work with the most amazing, high energy coworkers, they are truly the best. I would not change that for anything. Aside
from working, I like to spend my days with my wife and 4 dogs. On my days off, I like to listen to music, play video
games, and go on adventures with my family.
"""

[[staff.office]]
image = "images/staff/wendy-pena-rdh-800x800.jpg"
weight = 70
name = "Wendy Pena"
title = "Registered Dental Hygienist"
content = """
Hello, my name is Wendy Pena. I graduated from San Joaquin Valley College. I have a degree in Dental Hygiene. It has
been with great honor to join Dr. Nazari and his team. I enjoy mostly working with fellow dental health care
professionals that share my same passion in providing the best quality patient care. I began as a dental assistant in
2019, and I am now pleased to be a Dental Hygienist making sure the patients always have a comfortable and positive
experience. I am very passionate about treating patients with sensitivity to their dental needs. I consider myself a
lifelong learner.

Outside the office, I enjoy spending time with my family and going out and seeking adventurous things to do from trips,
to exploring new places.
"""

[[staff.office]]
image = "images/staff/jonathan-king-800x800.jpg"
weight = 80
name = "Jonathan King"
title = "Registered Dental Hygienist"
content = """
Hi, I'm Jonathan. I've been practicing dental hygiene since 2016, but I've been in the dental industry for 16 years. My main focus as a dental hygienist is providing periodontal treatment and educating patients on oral health. What brought me into dentistry were my family members. Many of my family members are in the dental field and created a lasting impression on me to provide dental care. My greatest accomplishment is when a returning patient values their oral health through consistent visits and routine at-home care.
"""


[cta]
enable = true

+++
