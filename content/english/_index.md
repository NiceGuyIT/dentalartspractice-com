+++
[menu.main]
name = "Home"
weight = 10

[banner]
enable = true
bg_image = "images/bg/slider-bg-01.jpg"
bg_overlay = true
title = "Dentistry for the Quality Conscious"
content = ""

[banner.button]
enable = false
#label = "Discover Our Project"
#link = "project"

[about]
enable = true
title = ""
description = """
"""
content = """
From the moment you walk into Dr. Nazari's dental practice, you'll know you're in an extraordinary practice. We are a multi-specialty practice, and we use the latest dental technology to treat patients with skill and expertise that comes from years of experience.

Our prosthodontist and periodontist are experts in their respective fields of dentistry, and they work closely with our general dentists to assure that you receive the best possible care. Choosing your dentist is a hard thing to do so please come by and we'll give you a tour of the office and introduce you to our staff.
"""

image = "images/bg/side-image-01.jpg"

[portfolio]
enable = true
bg_image = "images/bg/slider-bg-02.jpg"
title = "Smile! You're in GREAT hands."
content = """
We would like to welcome you to our office. We take great satisfaction in helping you maintain optimal oral health. Our
practice is devoted to comprehensive and preventive patient care.

Throughout our website, you will find an abundance of information about our practice, procedures we provide, and
dentistry in general. Please explore and learn as much about dentistry and our services as you desire. We believe our
patients should have as much information as possible in order to make important, informed decisions regarding their oral
health and treatment options.

Our patients are our most important asset, and we strive to develop long-lasting, trusting relationships with all of our
patients. Your referrals are welcome and appreciated.
"""

[portfolio.button]
enable = false
label = "View Works"
link = "project"

[service]
enable = true
# This one doesn't work.
title = "Our Services"

[cta]
enable = false
bg_image = "images/call-to-action-bg.jpg"
title = ""
content = """
"""

[cta.button]
enable = true
label = "Tell Us Your Story"
link = "contact"

[funfacts]
enable = false
title = ""
description = "''"

[[funfacts.funfact_item]]
icon = "ion-ios-chatboxes-outline"
name = "Cups Of Coffee"
count = "99"

[[funfacts.funfact_item]]
icon = "ion-ios-glasses-outline"
name = "Article Written"
count = "45"

[[funfacts.funfact_item]]
icon = "ion-ios-compose-outline"
name = "Projects Completed"
count = "125"

[[funfacts.funfact_item]]
icon = "ion-ios-timer-outline"
name = "Combined Projects"
count = "200"

[[funfacts.testimonial_slider]]
name = "Raymond Roy"
image = "images/clients/avater-1.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

[[funfacts.testimonial_slider]]
name = "Randi Renin"
image = "images/clients/avater-1.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

[[funfacts.testimonial_slider]]
name = "Rose Rio"
image = "images/clients/avater-3.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

+++
