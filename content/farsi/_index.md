+++
[menu.main]
name = "صفحه اصلی"
weight = 10

[banner]
enable = true
bg_image = "images/bg/slider-bg-01.jpg"
bg_overlay = true
title = "دندانپزشکی برای کیفیت آگاه"
content = ""

[banner.button]
enable = false
#label = "Discover Our Project"
#link = "project"

[about]
enable = true
title = ""
description = """
"""
content = """
از لحظه ای که وارد مطب دندانپزشکی دکتر نظری می شوید، متوجه می شوید که در یک مطب فوق العاده هستید. ما یک مطب چندتخصصی هستیم و از جدیدترین فناوری دندانپزشکی برای درمان بیماران با مهارت و تخصص که ناشی از سالها تجربه است استفاده می کنیم.

متخصص پروتز و پریودنتیست ما در زمینه های مربوط به دندانپزشکی خود متخصص هستند و از نزدیک با دندانپزشکان عمومی ما همکاری می کنند تا اطمینان حاصل کنند که شما بهترین مراقبت ممکن را دریافت خواهید کرد. انتخاب دندانپزشک کار سختی است، پس لطفا تشریف بیاورید تا از مطب بازدید کنید و شما را به کارکنان خود معرفی کنیم.
"""

image = "images/bg/side-image-01.jpg"

[portfolio]
enable = true
bg_image = "images/bg/slider-bg-02.jpg"
title = "لبخند! شما در دستان عالی هستید"
content = """
ما میخواهیم به شما در دفتر خود خوشامد بگوییم. ما از اینکه به شما در حفظ سلامت دهان و دندان مطلوب کمک می کنیم بسیار رضایت داریم. تمرین ما به مراقبت جامع و پیشگیرانه از بیماران اختصاص دارد.

در سراسر وب سایت ما ، اطلاعات زیادی در مورد عمل ، روش هایی که ارائه می دهیم و به طور کلی دندانپزشکی خواهید یافت. لطفاً تا آنجا که مایل هستید در مورد دندانپزشکی و خدمات ما اطلاعات بیشتری کسب کرده و بیاموزید. ما معتقدیم بیماران ما باید تا آنجا که ممکن است اطلاعات لازم را داشته باشند تا بتوانند در مورد گزینه های درمانی و بهداشت دهان و دندان خود تصمیمات مهم و آگاهانه بگیرند.

بیماران ما مهمترین دارایی ما هستند و ما تلاش می کنیم تا روابط طولانی مدت و قابل اعتماد با همه بیمارانمان ایجاد کنیم. از ارجاعات شما متشکریم.
"""

[portfolio.button]
enable = false
label = "مشاهده آثار"
link = "project"

[service]
enable = true
# This one doesn't work.
title = "خدمات ما"

[cta]
enable = false
bg_image = "images/call-to-action-bg.jpg"
title = ""
content = """
"""

[cta.button]
enable = true
label = "Tell Us Your Story"
link = "contact"

[funfacts]
enable = false
title = ""
description = "''"

[[funfacts.funfact_item]]
icon = "ion-ios-chatboxes-outline"
name = "Cups Of Coffee"
count = "99"

[[funfacts.funfact_item]]
icon = "ion-ios-glasses-outline"
name = "Article Written"
count = "45"

[[funfacts.funfact_item]]
icon = "ion-ios-compose-outline"
name = "Projects Completed"
count = "125"

[[funfacts.funfact_item]]
icon = "ion-ios-timer-outline"
name = "Combined Projects"
count = "200"

[[funfacts.testimonial_slider]]
name = "Raymond Roy"
image = "images/clients/avater-1.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

[[funfacts.testimonial_slider]]
name = "Randi Renin"
image = "images/clients/avater-1.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

[[funfacts.testimonial_slider]]
name = "Rose Rio"
image = "images/clients/avater-3.jpg"
designation = "CEO-Themefisher"
content = "This Company created an e-commerce site with the tools to make our business a success, with innovative ideas we feel that our site has unique elements that make us stand out from the crowd."

+++
