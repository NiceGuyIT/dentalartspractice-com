+++
title = "پزشکان ما"
draft = false
# Page title background image
bg_image = "images/bg/page-title.jpg"
# Meta description shown below the page title.
description = "با پزشکان در هنرهای دندانپزشکی ملاقات کنید."

[menu.main]
name = "پزشکان"
identifier = "پزشکان"
weight = 30

[menu.footer]
name = "پزشکان"
identifier = "پزشکان"
weight = 30

+++
