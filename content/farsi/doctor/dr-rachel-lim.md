+++
title = "Dr. Rachel Lim"
# Shown below the title in the header
description = "**اندودنتیست**، DMD"
draft = false
bg_image = "images/bg/page-title.jpg"

# Not used
course = ""

# teacher portrait
image = "images/doctor/dr-rachel-lim-703x800.jpg"

# Weight for sorting
weight = 40

# type
type = "doctor"

+++

سلام، نام من دکتر راشل لیم است. من آموزش تخصصی و مدرک خود را در زمینه ریشه دندان از دانشگاه کلمبیا دریافت کردم. قبل از آموزش تخصصی، دوره رزیدنتی را در دندانپزشکی عمومی از دانشگاه پاسیفیک به پایان رساندم و به مدت پنج سال به عنوان دندانپزشک عمومی در لس آنجلس مشغول به کار شدم. من از دانشکده دندانپزشکی با رتبه ممتاز از دانشگاه علوم بهداشت غربی فارغ التحصیل شدم و به انجمن افتخاری Omicron Kappa Upsilon معرفی شدم. دکتر لیم در اورنج کانتی بزرگ شد و از کاوش در شهرهای جدید، پیاده روی و گذراندن زمان با کیفیت با عزیزان لذت می برد.