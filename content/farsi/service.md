+++
title = "خدمات"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "service"
draft = false

[menu.main]
name = "خدمات"
weight = 20

[menu.footer]
name = "خدمات"
weight = 20

############################## About Service ##############################
[about]
enable = false
title = ""
content = """
"""
image = "images/company/company-group-pic.jpg"

# Should be no more than 3 features services.
[featured_service]
enable = false

############################## Featured Service ###########################
[[featured_service.service_item]]
name = "Featured Service 1"
icon = "magic"
color = "primary"
content = ""

[[featured_service.service_item]]
name = "Featured Service 2"
icon = "image"
color = "primary-dark"
content = ""

[[featured_service.service_item]]
name = "Featured Service 3"
icon = "leaf"
color = "primary-darker"
content = ""


############################## Service ####################################
[service]
enable = true
title = "خدمات ما"
description = ""

[[service.service_item]]
icon = "magic"
icon_color = "#0000cd"
category = "ایمپلنت های دندانی"
name = "ایمپلنت های دندانی"
content = """
کاشت دندان یک راه حل دائمی و جذاب برای جایگزینی دندان های از دست رفته یا کشیده شده است.
"""

[[service.service_item]]
icon = "teeth"
icon_color = "#ad76ff"
category = "بهداشت دندان"
name = "بهداشت دندان"
content = """
در حالی که در مطب ما هستیم ، ما اطمینان می دهیم که شما بالاترین سطح خدمات را دریافت می کنید و اطمینان حاصل می کنیم که کار دندانپزشکی ما از بالاترین کیفیت برخوردار است.
"""

[[service.service_item]]
icon = "image"
icon_color = "#ffd700"
name = "لوازم آرایشی"
category = "روکش"
content = """
روکش ، سفید کننده ، باندینگ و پر کننده های سفید ، منبت و آنلی
"""

[[service.service_item]]
icon = "cubes"
icon_color = "#800080"
name = "مقوی"
category = "پل ها"
content = """
پوئنتس، کوروناس، دنتادوراس، اندودونسیاس
"""

[[service.service_item]]
icon = "tooth"
icon_color = "#87cefa"
name = "پریودنتیک"
category = "آرستین"
content = """
لیزر، بیوپسی، پیوند استخوان، تقویت برآمدگی، افزایش سینوس، قرار گرفتن در معرض سگ، افزایش طول تاج، فرنکتومی، پیوند لثه / لثه
"""

[[service.service_item]]
icon = "mortar-pestle"
icon_color = "#778899"
name = "ارتودنسی"
category = "پاک کردن صحیح"
content = """
ClearCorrect، Sparks، Clear Aligners، Night Guard، TMJ
"""

[[service.service_item]]
icon = "stethoscope"
icon_color = "#7fe1a6"
name = "اندودنتیست"
category = ""
content = """
قدامی کانال ریشه، دو لخت و مولر، درمان مجدد کانال های ریشه، اپیکوکتومی، دبریدمان پالپ (باز و مداد)
"""


############################## Call to Action #############################
[cta]
enable = true

+++
