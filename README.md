# Dental Arts Practice

[DentalArtsPractice.com](https://DentalArtsPractice.com) website.

## Build

Use this command to build the Docker image. This needs more work so it can be used for dev.

```bash
docker buildx build --tag dentalartspractice-com .
docker run --volume $"($env.PWD):/site" dentalartspractice-com
```

## With Cherry

Cherry does not support RTL and has no plans to.

> Unfortunately, we currently do not have plans to support RTL languages and cannot set the language
> of the practice portal based on browser language. However, we appreciate your feedback and will
> keep this in mind as we continue to develop and improve on the Cherry experience.
