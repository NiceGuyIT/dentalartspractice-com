# Use Alpine Linux as our base image so that we minimize the overall size our final container, and minimize the surface area of packages that could be out of date.
FROM alpine:latest

LABEL description="Docker container for building static sites with the Hugo static site generator."
# Most of this is from here: https://github.com/jojomi/docker-hugo/blob/master/Dockerfile
# Some is taking from the official repo: https://github.com/gohugoio/hugo/blob/master/Dockerfile

# config
ENV HUGO_VERSION=0.125.3
#ENV HUGO_TYPE=
ENV HUGO_TYPE=_extended

RUN apk add --update git asciidoctor libc6-compat libstdc++ curl \
    && apk upgrade \
    && apk add --no-cache ca-certificates

ENV HUGO_ID=hugo${HUGO_TYPE}_${HUGO_VERSION}
RUN curl --location --output - https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_ID}_linux-amd64.tar.gz | tar --extract --gzip --directory=/usr/local/bin/ hugo

VOLUME /site
WORKDIR /site

# Expose port for live server
EXPOSE 1313

CMD ["hugo", "server", "--disableFastRender", "--buildDrafts", "--bind", "0.0.0.0", "--port", "1313"]

