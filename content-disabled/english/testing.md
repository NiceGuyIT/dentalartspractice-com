+++
title = "Testing"
description = ""
bg_image = "images/bg/page-title.jpg"
layout = "testing"
draft = false
+++

Shortcode without markdown processing: "{{< fontawesome icon="wheelchair" >}}"

Shortcode with markdown processing: "{{< fontawesome wheelchair >}}"

