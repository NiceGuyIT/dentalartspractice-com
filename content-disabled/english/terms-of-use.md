+++
title = "Terms of Use"
description = ""
subtitle = ""
lastmod = "2021-10-09T00:00:00"
bg_image = "images/bg/page-title.jpg"
draft = false

[menu.footer]
name = "Terms of Use"
weight = 200

+++

These Terms of Use (the “Terms of Use”)  govern the use of the services (the "Service") offered by _Dental Arts
Practice_ (“Dental Arts Practice,” “us” or “we” or “our”) at the Web Site, <http://dentalartspractice.com> (the “Site”).
Please read these Terms of Use before using or continuing to use the Site or Service. Use of the Site constitutes an
agreement to all terms and conditions in these Terms of Use. By using this Site or the Service you represent and warrant
that you accept and agree to be bound by all of the terms and conditions contained herein and covenant to comply with
these Terms of Use whether you are a visitor or a Member (as defined below). If you object to anything in these Terms of
Use, do not use the Site or the Service. We reserve the right to modify and update these Terms of Use at any time
provided that we give notice to you of any changes via email and/or by posting the amended Terms of Use on the Site.
Your use of the Service after such posting, or your failure to terminate your account before the effective date of the
revised version, will constitute acceptance by you of the revised version of the Terms of Use.

## Privacy

Use of the Site and Service is also governed by our Privacy Policy located at the link in the bottom of our
website <http://dentalartspractice.com> incorporated into these Terms of Use by this reference.

In order to cooperate with legitimate governmental requests, subpoenas or court orders, to protect our systems and
customers, to ensure the integrity and operation of our business and systems and to improve the Site and Service, we may
access and disclose any information and content (either in your account profile or in the course of using the Service)
we consider necessary or appropriate, including, without limitation, profile information (i.e. name, e-mail address,
etc.), IP addressing and traffic information, usage history, and posted content.

## General Terms & Transactions

**Disputes Between Members.** If there are any issues between Members concerning advice, services received or payment due,
Members must deal directly with the other Member to resolve any disputes. Publishers will not be held responsible and
expressly disclaims any liability whatsoever for any claims and/or controversies that may arise for any disputes between
Members.

**Commercial Use Prohibition.**  The Site is for the use of individual Members only and may not be used in connection with any commercial endeavors (other than obtaining Chiropractic or Dental services) without the express written consent of us in advance. Organizations, companies, and/or businesses may only register as an Agency or advertiser and may not use the Service or the Site for any illegal or unauthorized purpose, including, but not limited to, collecting usernames and/or email addresses of members by electronic or other means, unless express written permission from us is obtained in advance.

## Your Account

**Creating An Account.**  When you register for a profile, you must complete the registration process by providing us
with current, complete and accurate information as prompted by the applicable registration form. The information is used
to identify you as a unique Member. In the event that we learn that you have provided us with false or misleading
information, we may immediately, without notice, terminate your profile.

**Account Information is Unique to You.** During the registration process, you may be asked to create a unique profile name
and a password that are specific to your profile. You may not use a profile name that is used by someone else, is vulgar
or otherwise offensive (as determined by us), infringes any trademark or other proprietary rights of others, or is used
in any way that violates these Terms of Use or any applicable codes of conduct or posted rules, instructions or
guidelines regarding the Service. You may not use anyone else’s profile at any time. Your profile is personal to you and
you may not transfer to make available your profile to others. You are entirely responsible for maintaining the
confidentiality of your profile information (including account names and passwords). Furthermore, you are entirely
responsible for any activity that occurs under your account. You agree to notify immediately of any unauthorized use or
theft of your profile or any other breach of security. will not be liable for any loss that you may incur as a result of
someone else using your account, either with or without your knowledge. You will be liable for losses incurred by or
another party due to someone else using your profile.

**Termination of Your Account.**  You may delete your account at any time. Additionally, may terminate your account with
the Site for any reason (including, without limitation, any breach of these Terms of Use or any applicable policy posted
on the Site from time to time), effective upon sending notice to you at the then-current e-mail address in yourprofile.
You understand that termination of your profile, whether by us or by you, may involve deletion of your profile
information from our databases. We will not have any liability whatsoever to you for any termination of your profile or
related deletion of you information. We reserve the right to take appropriate legal action, including without limitation
pursuing civil, criminal, and injunctive redress in the event that any Member violates these Terms of Use. All terms
that by their nature may survive termination of your account shall be deemed to survive such termination.

## Conduct Within Public Areas of Site

The Site may contain e-mail services, bulletin board services, forums, communities or other message or communication
facilities designed to enable you to communicate and interact with other Members (the "Public Areas"). You agree to use
the Public Areas only to post, send and receive messages and materials that are proper and, when applicable, related to
the particular Public Area. We have no obligation to monitor the Public Areas, provided, however, we reserve the right
to review materials posted to the Public Areas and to remove any materials at any time, without notice, for any reason
and in our sole discretion. We reserve the right to terminate or suspend your access to any or all of the Public Areas
at any time, without notice, for any reason whatsoever. You acknowledge that postings and other communications by
Members are not controlled or endorsed by us, and such communications shall not be considered reviewed, screened or
approved by us. Statements made in postings, forums, bulletin boards and other Public Areas reflect only the views of
their authors. We specifically disclaims any liability with regard to the Public Areas and any actions resulting from
your participation in any Public Areas.

You acknowledge and agree that your communications with other Members via the Public Area or otherwise is public and not
private communications, and that you have no expectation of privacy concerning your use of the Public Areas. You
acknowledge that personal information that you communicate on the Public Areas may be seen and used by others and result
in unsolicited communications; therefore, WE STRONGLY ENCOURAGE YOU NOT TO DISCLOSE ANY PERSONAL INFORMATION ABOUT
YOURSELF THROUGH THE PUBLIC AREAS. We are not responsible for information that you choose to communicate to other
Members via the Public Areas, or for the actions of other Members.

## Acceptable Use Policy

You may not use the Site or Service for any illegal or unauthorized purpose and you must not abuse, harass, threaten,
impersonate or intimidate other Members. You are solely responsible for your conduct and any content that you submit,
post or display on the Site. In furtherance of the foregoing, and by way of example and not as a limitation, you agree
that you may not access the Site or use the Service in order to:

- Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as, but not limited to, rights of
  privacy and publicity) of others.
- Publish, post, upload, distribute or disseminate any profane, defamatory, infringing, obscene or unlawful topic, name,
  material or information.
- To recruit for another website or service, solicit, advertise, or contact in any form Members for employment,
  contracting, or any other purpose for a business not affiliated with us without express written permission from us.
- Use the Site or Service for any purpose which is in violation of local, state, national, or international law.
- Upload files that contain software or other material that violates the intellectual property rights (or rights of
  privacy or publicity) of any third party.
- Post content that contains viruses, Trojan Horses, worms, time bombs, corrupted files or data, or any other similar
  software or Services that may damage the operation of the Service or another's computer.
- Conduct or forward surveys, contests, pyramid schemes, or chain letters.
- Post the same note repeatedly (referred to as 'spamming'). Spamming is strictly prohibited.
- Download any file posted by another Member that a Member knows, or reasonably should know, cannot be legally
  distributed through the Site.
- Restrict or inhibit any other Member from using and enjoying the Site or Public Areas.

## Information Provided by Members

You are solely responsible for any information or materials that you post on or transmit through the Site, and we merely
acts as a passive conduit for your online distribution and publication of your information for the benefit of other
Members. With respect to any information or materials that you post on or transmit through the Site, you hereby
represent and warrant to us that (i) you are the owner or a licensee or otherwise have the right to provide such
information, (ii) such information shall not be false, inaccurate, misleading or fraudulent; (iii) such information
shall not infringe any third party's copyright, patent, trademark, trade secret or other proprietary right or rights of
publicity or privacy; (iv) such information shall not violate any law, statute, ordinance, or regulation (including
without limitation those governing export control, consumer protection, unfair competition, anti-discrimination or false
advertising); (v) such information shall not be defamatory, libelous, unlawfully threatening, or unlawfully harassing; (
vi) such information shall not be obscene or contain child pornography or be harmful to minors; (vii) such information
shall not contain any viruses, Trojan Horses, worms, time bombs, cancelbots or other computer programming routines that
are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or
personal information; and (viii) such information shall not create liability for us or cause us to lose (in whole or in
part) the services of our ISPs or other partners or suppliers. You hereby grant us a non-exclusive, worldwide,
perpetual, irrevocable, fully-paid, royalty-free, sublicensable (through multiple tiers) right to exercise the
copyright, publicity rights, and any other rights you have in any information or materials that you post on or through
the Site, in any media now known or not currently known.

## 3rd Party Links

The Site may contain links from us to third-party sites on the Internet (“Third Party Sites”). Your use of all such
Third Party Sites are at your own risk it is the responsibility of each Member to evaluate the content and usefulness of
the information obtained from Third Party Sites. Members further acknowledge that use of any Third Party Sites is
governed by the terms and conditions of use for those websites, and not by our Terms of Use. We are not responsible for
the content, business practices or privacy policies, or for the collection, use or disclosure of any information that
the Third Party Sites may collect. To the extent such links are provided by us, they are provided only as a convenience,
and such link to a Third Party Site does not imply our endorsement, adoption or sponsorship of, or affiliation with,
such Third Party Sites. We do not accept any responsibility for reviewing changes or updates to, or the quality,
content, policies, nature or reliability of Third Party Sites, or websites linking to the Site.

## Disclaimer of Warranties

We are not responsible for any incorrect or inaccurate content posted on the Site. We are not responsible for the
conduct, whether online or offline, of any Member/User while using the Services. Under no circumstances shall we be
responsible for any loss or damage, including personal injury or death, resulting from use of the Services or from any
content posted on the Site by us or transmitted to Members/Users, or any interactions between Members/Users, whether
online or offline.

USE OF THIS SITE IS AT THE USER'S SOLE RISK AND USE OF THE SERVICE IS AT THE MEMBER’S/USERS SOLE RISK. THE SITE AND
SERVICE IS PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT
LIMITED TO, WARRANTIES OF TITLE OR IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
NON-INFRINGEMENT. WE DO NOT WARRANT OR GUARANTEE THE TIMELINESS, ACCURACY OR COMPLETENESS OF THE SITE, THE INFORMATION
APPEARING ON THE SITE OR THAT THE SERVICE WILL MEET YOUR REQUIREMENTS. WE DO NOT WARRANT OR GUARANTEE THAT ACCESS TO THE
SITE WILL BE UNINTERRUPTED OR THAT THE SITE WILL BE ERROR-FREE; NOR DO WE MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY
BE OBTAINED FROM THE USE OF THE SITE, OR AS TO THE ACCURACY, RELIABILITY OR CONTENT OF ANY INFORMATION OR SERVICE
PROVIDED THROUGH THE SITE. WE ARE NOT RESPONSIBLE FOR THE CONDUCT, WHETHER ONLINE OR OFFLINE, OF ANY USER OF THE SITE OR
SERVICE.

## Indemnification

By accessing this Site, you agree to indemnify, defend, and hold harmless _Dental Arts Practice_, the site’s hosting
company, publishers and its affiliates and each of their respective directors, officers, employees, agents, licensors,
attorneys, independent contractors and providers (collectively referred to herein as “Indemnified Parties”) from and
against any and all claims, losses, expenses or demands of liability, including attorneys' fees and costs incurred by
any us. Indemnified Party in connection with any claim (including any third-party intellectual property claim) arising
out of: (i) information, materials and content submitted, posted or transmitted by you on or through the Site, or (ii)
your use of the Site and Service as a whole, and (iii) your breach of any provision of these Terms of Use. You further
agree that you will cooperate with us as is reasonably required in the defense of such claims. Indemnified Parties and
affiliates reserve the right, at their own expense, to assume the exclusive defense and control of any matter otherwise
subject to your foregoing indemnification obligations, and you shall not, in any event, settle any claim or matter
without the written consent of us.

## Release

You hereby release us and affiliates and each of their respective directors, officers, employees, agents, licensors,
attorneys, independent contractors and providers from all claims, demands, or damages (actual or consequential) of every
kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way
connected with  (i) any information, content, statement or materials delivered through Site but which was posted or
otherwise provided by a Member or Third-party Site, or (ii) your use of the Site or Service.

If you are a California resident, you hereby waive California Civil Code Section 1542, which states: "A general release
does not extend to claims which the creditor does not know or suspect to exist in his favor at the time of executing the
release, which, if known by him must have materially affected his settlement with the debtor."

## Limitation of Liability

YOU ACKNOWLEDGE AND AGREE THAT WE ARE NOT LIABLE FOR ANY ACT OR FAILURE TO ACT OR CONTENT ON THE SITE OR ARISING OUT OF
THE SERVICE. YOU ACKNOWLEDGE AND AGREE THAT YOUR SOLE AND EXCLUSIVE REMEDY FOR ANY DISPUTE WITH US IS TO STOP USING THE
SITE AND SERVICE. WE DO NOT ENDORSE, WARRANT OR GUARANTEE ANY THIRD-PARTY PRODUCT OR SERVICE OFFERED THROUGH THE SITE
AND WILL NOT BE A PARTY TO OR IN ANY WAY BE RESPONSIBLE FOR MONITORING ANY TRANSACTION BETWEEN YOU AND THIRD-PARTY
PROVIDERS OF PRODUCTS OR SERVICES.

IN NO EVENT SHALL WE BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY LOST PROFIT OR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY,
INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES ARISING FROM YOUR USE OF THE SITE OR SERVICE, EVEN IF WE HAVE BEEN ADVISED OF
THE POSSIBILITY OF SUCH DAMAGES. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN, _Dental Arts Practice_’s
LIABILITY TO YOU FOR ANY DAMAGES ARISING FROM OR RELATED TO (FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE
ACTION), WILL AT ALL TIMES BE LIMITED TO ONE HUNDRED US DOLLARS ($100).

## Intellectual Property Rights

All text, graphics, editorial content, data, formatting, graphs, designs, HTML, look and feel, photographs, music,
sounds, images, software, videos, designs, typefaces and other content (collectively “Proprietary Material”) that users
see or read on the Site is owned or licensed by us or the hosting company. Proprietary Material is protected in all
forms, media and technologies now known or hereinafter developed. Except as otherwise expressly provided herein, Members
or users may not copy, download, use, redesign, reconfigure, or retransmit anything from the Site without our or the
hosting company’s prior express written permission. Furthermore, Members/users are not allowed to post or distribute any
material that they do not own, or which they do not have permission to use. Violation of this policy may result in
copyright, trademark or other intellectual property rights violations and liability, and subject Members to termination
from the use of this Site or civil or criminal penalties.

## Limitations on Use

In the interest of maintaining the performance and availability of the Site and in enforcing these Terms of Use, we
reserve the right to place certain limitations on Member's/Users access to the Site and certain site features. These
limitations may include (but are not limited to) the number of messages sent through the system and the number of
Members/Users to whom a message can be sent. Members/Users acknowledge that this term supersedes any specific offer made
by us and that these limitations may be enforced at our sole discretion.

## Arbitration

Any controversy or claim arising out of or relating to an alleged breach of these Terms of Use or the operation of this
Site shall be settled by binding arbitration in accordance with the commercial arbitration rules of the American
Arbitration Association before a single arbitrator. Any such controversy or claim shall be arbitrated on an individual
basis, and shall not be consolidated in any arbitration with any claim or controversy of any other party. The
arbitration shall be conducted in Walnut, CA. Either you or us may seek any interim or preliminary relief from a court
of competent jurisdiction necessary to protect the rights of property for you or _Dental Arts Practice_ pending the
completion of arbitration. The arbitrator shall not have the authority, power, or right to alter, change, amend, modify,
add, or subtract from any provision of these Terms Of Use or to award punitive damages. The arbitrator shall have the
power to issue mandatory orders and restraining orders in connection with the arbitration. The award rendered by the
arbitrator shall be final and binding on the parties, and judgment may be entered thereon in any court of competent
jurisdiction.

## General Provisions

Failure by us to enforce any provision(s) of these Terms of Use shall not be construed as a waiver of any provision or
right. These Terms of Use, and all other aspects of use of the Site, shall be governed by and construed in accordance
with the laws of the State of CA, without regard to its conflict of laws rules. Any controversy or claim that is not
subject to the Arbitration provision set forth in these Terms of Use, shall be exclusively brought in the federal,
state, or local courts located in Walnut, CA; and, with regard to such claims and disputes, Members hereby irrevocably
(i) submit to the exercise of personal jurisdiction over them by these courts, and (ii) waives any jurisdictional,
venue or inconvenient forum objections to such courts. These Terms of Use, and any additional terms referenced herein,
constitute the entire agreement between Members/Users and _Dental Arts Practice_ with respect to the Site and Service. If
any provision of these Terms of Use is found to be invalid or unenforceable, the remaining provisions shall be enforced
to the fullest extent possible, and the remaining Terms of Use shall remain in full force and effect. These Terms of Use
inure to the benefit of _Dental Arts Practice_ its successors and assigns. Members/users are not legally affiliated with
us in any way merely by virtue of their membership or usage of our Site or Service other than as might be described by
these Terms of Use, and no independent contractor, partnership, joint venture, employer-employee or
franchiser-franchisee relationship is intended or created by these Terms of Use. We are not an employment service and
does not serve as an employer of any Member that may use this Site.

Copyright/Trademark Information. All rights reserved. The trademarks, logos and service marks ("Marks") displayed on the
Site are our property or the property of other third parties. You are not permitted to use these Marks without our or
the hosting company’s prior written consent or the consent of such third party which may own the Mark.

Last updated: July 2011
