+++
title = "Services"
description = ""
bg_image = "images/slider-bg-01.jpg"
layout = "service"
draft = false

[menu.main]
name = "Services"
weight = 20

[menu.footer]
name = "Services"
weight = 20

############################## About Service ##############################
[about]
enable = true
title = "Creative UX/UI Design Agency"
content = """
This is the about section in the services page. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate
soluta corporis odit, optio cum! Accusantium numquam ab, natus excepturi architecto earum ipsa aliquam, illum, omnis
rerum, eveniet officia nihil. Eum quod iure nulla, soluta architecto distinctio. Nesciunt odio ullam expedita, neque
fugit maiores sunt perferendis placeat autem animi, nihil quis suscipit quibusdam ut reiciendis doloribus natus nemo id
quod illum aut culpa perspiciatis consequuntur tempore? Facilis nam vitae iure quisquam eius harum consequatur sapiente
assumenda, officia voluptas quas numquam placeat, alias molestias nisi laudantium nesciunt perspiciatis suscipit hic
voluptate corporis id distinctio earum. Dolor reprehenderit fuga dolore officia adipisci neque!
"""
image = "images/company/company-group-pic.jpg"

# Should be no more than 3 features services.
[featured_service]
enable = true

############################## Featured Service ###########################
[[featured_service.service_item]]
name = "Interface Design"
icon = "ion-erlenmeyer-flask"
color = "primary"
content = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe enim impedit repudiandae omnis est temporibus."

[[featured_service.service_item]]
name = "Product Branding"
icon = "ion-leaf"
color = "primary-dark"
content = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe enim impedit repudiandae omnis est temporibus."

[[featured_service.service_item]]
name = "Game Development"
icon = "ion-lightbulb"
color = "primary-darker"
content = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe enim impedit repudiandae omnis est temporibus."


############################## Service ####################################
[service]
enable = true
title = "Our Services"
description = "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, <br> there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics"

[[service.service_item]]
icon = "ion-wand"
category = "Dental Implants"
name = "Implants"
content = """
Dental implants are a permanent and appealing solution to replace missing or extracted teeth. They are better than other
alternatives like bridges because no additional teeth need to be altered to place the new tooth.
"""

#[[service.service_item]]
#icon = "ion-ios-nutrition"
#category = "Dental Hygiene"
#name = "Hygiene"
#content = """
#While at our office, we make sure that you receive the highest level of service and ensure that our dental work is of
#the highest quality. To ensure that you maintain great oral health, this level of quality needs to extend into your
#personal oral hygiene routine. We can help you establish a dental hygiene routine that will keep your teeth healthy and
#white. If you have any questions about your current hygiene plan please ask us.
#"""

[[service.service_item]]
icon = "ion-image"
name = "Cosmetic"
category = "Veneers"
content = """
Veneers are an excellent way to redesign and reshape your entire mouth. Veneers are a very thin ceramic shell that
covers your existing tooth structure. In placing these new veneers on your teeth, you are able to fix the look of issues
such as staining, cracked teeth, spaces, misaligned teeth, chipped teeth and many other issues.
"""

[[service.service_item]]
icon = "ion-paintbrush"
name = "Endodontics"
category = "Root Canal"
content = """
Endodontics is the dental specialty that deals with the nerves of the teeth. Root canals are probably the most notorious
procedure in dentistry and the most common procedure relating to endodontics. When a tooth becomes infected it is
usually related to the nerves in the root of the tooth. The infected nerves need to be removed. If left untreated an
infection can turn into an abscess, which is a much more serious problem that includes bone loss in the jaw.
"""

[[service.service_item]]
icon = "ion-waterdrop"
name = "Restorative"
category = "Bridges"
content = """
Dental bridges are a great way to replace missing teeth. Your existing teeth are used to literally create a bridge to
cross the area where your tooth is missing. Bridges are made from gold, metal, alloys, or porcelain to ensure that they
are strong and durable.
"""

[[service.service_item]]
icon = "ion-ios-football"
name = "Pediatric"
category = "Sealants"
content = """
Sealants are a great way to protect against tooth decay and cavities on your back teeth (molars). These are the teeth
that are most vulnerable to cavities and decay because they are used in the chewing process, and are the most difficult
to reach and clean. Molars first come in at around 5-7 years of age, with a second set coming in between the ages of
11-14. It is best to have a sealant placed when the molars first come in to ensure they are protected early.
"""

[[service.service_item]]
icon = "ion-leaf"
name = "Periodontic"
category = "Arestin"
content = """
You may have heard about gum disease, also known as "periodontitis" or "periodontal disease." Maybe a dental
professional or hygienist recently told you that you have this infection.
"""

[[service.service_item]]
icon = "ion-erlenmeyer-flask"
name = "Oral Surgery"
category = "Wisdom Teeth"
content = """
Wisdom teeth are the third and final set of molars that emerge, usually during your late teens to early twenties. For
some people the wisdom teeth emerge through the gums and have enough room to grow in naturally. For others, wisdom teeth
often cause problems as they are trying to protrude through the gums. When a wisdom tooth is impacted the tooth is
coming in at an angle and not straight through the gum line. This can cause pain, the tooth can come in unevenly, or the
tooth may only emerge partially.
"""

[[service.service_item]]
icon = "ion-ios-snowy"
name = "Orthodontic"
category = "Clear Correct"
content = """
Clear Correct is a new system of straightening teeth without the use of conventional braces. A series of clear plastic
aligners are utilized to create tooth movement. Moving teeth with removable aligners is not new. However, the computer
program, which can generate a series of aligners with small changes is the new part. Clear Correct is recommended for
orthodontic situations with mild to moderate spacing or crowding. They are virtually undetectable, easy to use and
comfortable to wear.
"""


############################## Call to Action #############################
[cta]
enable = true

+++

# Implants

## Dental Implants

Dental implants are a permanent and appealing solution to replace missing or extracted teeth. They are better than other
alternatives like bridges because no additional teeth need to be altered to place the new tooth.

The entire implant process is performed over the course of a few months. The first part of the process is to install the
implant itself, where a screw is placed into the jaw bone. An incision is made in the gum so that the implant can be
inserted. Multiple implants can be placed at once if necessary. After the implants are placed the gums are sutured.

The implant must be allowed about 3-6 months to heal, and during this time the jaw bone will form around the implant in
a process called osseointegration. During this healing time you can have temporary crowns installed so that you can eat
and speak normally and maintain a proper aesthetic appearance for your smile.

After the implant has healed it is time to place an abutment on the implant. The abutment serves as the base for your
new tooth. One this is placed an impression of the abutment is taken and is used to create your permanent restoration.
Some offices have an onsite lab to create the crown, but others will have to send it to an outside lab. Once the
restoration is completed you can return to the office to attach the restoration permanently. Your smile will look just
like it used to, and after a short period of getting used to the implant it will feel just like one of your own teeth.

## Dental Implant FAQ's

- Q. What is a dental implant?
- A. A dental implant is a permanent new root for a missing tooth. It is a titanium screw that is placed into the jaw
  bone. An abutment and crown is then placed on the implant to provide a new tooth that is permanent and looks just like
  a natural tooth.

- Q. How are dental implants different than dentures?
- A. Dentures are a removable set of teeth or partial set of teeth that need to be removed and cared for in a special
  manner. They can often feel bulky and are difficult to get used to. Dental implants are a permanent replacement for
  missing teeth that can be cared for just like natural teeth.

- Q. What are the benefits of dental implants?
- A. Dental implants look and feel just like real teeth, they are permanent, you can eat the foods you want without any
  fear of embarrassment, and the procedure is very successful and fairly inexpensive.

- Q. Who can place a dental implant?
- A. Your dentist may be able to perform implants in their own office based on their training. If not, they will
  recommend you to an implantologist, cosmetic dentist, or another specialist that will perform the procedure.

- Q. How much do implants cost?
- A. The price of your dental implants will vary based on the level of experience of your doctor, how many implants you
  are having placed, amount of insurance coverage, and more. Schedule a consultation with us to determine a price for
  your implants and a financing plan if necessary.

- Q. Do dental implants hurt?
- A. No. A local anesthetic will be given before the procedure so that you do not feel any pain. If you have anxiety or
  fear you should discuss sedation options with your dentist.

- Q. How long do implants last?
- A. When cared for properly, implants should last a lifetime. Proper oral hygiene and regular dental visits are
  essential to the success of your implant.

# Dental Hygiene

While at our office, we make sure that you receive the highest level of service and ensure that our dental work is of
the highest quality. To ensure that you maintain great oral health, this level of quality needs to extend into your
personal oral hygiene routine. We can help you establish a dental hygiene routine that will keep your teeth healthy and
white. If you have any questions about your current hygiene plan please ask us.

Your teeth are not the only important part of your mouth. Your gums are essential to oral hygiene as well. We can
provide periodontal cleanings and treatment, or refer you to one of our recommended specialists. Please let us know if
you have any questions.

# Cosmetic

## Veneers

Veneers are an excellent way to redesign and reshape your entire mouth. Veneers are a very thin ceramic shell that
covers your existing tooth structure. In placing these new veneers on your teeth, you are able to fix the look of issues
such as staining, cracked teeth, spaces, misaligned teeth, chipped teeth and many other issues.

In terms of the process to place veneers, it is a process that takes approximately two to three visits. In the first
visit, we will discuss the new shape and look of what we are going to accomplish by placing the veneers. Once we have
put a plan in place, we will carefully prepare the teeth for the placement of the new veneers. As we complete this piece
of the process, the next step is to take an impression of the newly prepared teeth. This impression will be sent to our
laboratory for the final product to be fabricated. We only utilize the highest standard laboratory for fabrication of
all of our work. This process at the laboratory takes between 10-14 days. While the new veneers are being fabricated at
the lab, we will provide you with a beautiful temporary solution to wear for this time.

As the veneers are finalized, we will make every effort to make sure that your new veneers will feel just like your
natural teeth. We will bond them into place ensuring that you can enjoy your new smile for a very long time!

## Whitening

Keeping our teeth their whitest is a lot harder than it sounds. With all the coffee, wine, smoking and other foods that
have the ability to stain our teeth on a daily basis, even proper maintenance sometimes leaves them a little lackluster.
Teeth whitening is an excellent way to restore the natural color of your teeth or even make them whiter than your
natural color if you would like.

To accomplish the whitening of your teeth, we utilize a tray whitening system. This tray whitening system is completed
in 2 steps. The first step is to make an impression of your teeth. With this impression, we'll craft you custom
whitening trays that you can use over and over. Finally, you will take the whitening gel and put it in the gel for a
short period of time over a period of a few days. This often results in a whiter smile of 4 - 8 shades!

Please contact us today to see if you are a candidate for this type of whitening

## Bonding and White Fillings

Bonding is a popular method to enhance the aesthetics of your smile. Bonding can be used to correct cracks or gaps in
teeth, as a filling after a cavity has been removed, or to cover up stains or discolored teeth.

A composite resin is used on the affected tooth or teeth. It is molded and sculpted over an adhesive gel that is placed
on the tooth. After the resin has been applied an, LED light is used to harden the resin, which is then polished to give
you a fresh, new smile.

Bonding is an obvious improvement over unsightly silver amalgam fillings. With the advancements in dental technology,
bonding usually lasts for over 10 years. It is a safe, affordable, and attractive solution for many dental problems.

## Inlays and Onlays

Inlays and onlays are often referred to as partial crowns. They use the existing tooth as a base and fit the inlay or
onlay onto the tooth. This is done to strengthen the tooth, restore its shape, and prevent further damage. An inlay is
done when there is no damage to the cusps of the tooth and the inlay can be placed right on the tooth. An inlay is used
when the damage is a little more extensive.

The decayed area of the tooth is first removed during the procedure. A mold of the tooth is then taken and sent to a
dental lab. They create a restoration made from porcelain, gold, or a composite resin. The restoration takes about 2-3
weeks to make, so a temporary inlay or onlay will be placed on the tooth for that time. During your next visit the inlay
or onlay will be placed into your mouth and set with cement. Your tooth will look natural and you or anyone else won't
be able to tell the difference.

# Endodontics

## Root Canal

Endodontics is the dental specialty that deals with the nerves of the teeth. Root canals are probably the most notorious
procedure in dentistry and the most common procedure relating to endodontics. When a tooth becomes infected it is
usually related to the nerves in the root of the tooth. The infected nerves need to be removed. If left untreated an
infection can turn into an abscess, which is a much more serious problem that includes bone loss in the jaw.

The area around the tooth is numbed with a local anesthetic to start the procedure. The dentist will then drill down
into the tooth to create an opening into the canal. They will then be able to remove infected tissue and clean the
canal. After the infection has been removed, the space if filled with a sealant called gutta percha. It is highly
recommended that a tooth that has undergone a root canal is fitted with a crown. This will improve the appearance of the
tooth, and will also make it much more likely that the root canal is successful.

"Root canal" has become a scary term for dental patients to hear, but the benefits of the procedure and advances in
dental technology have made it much less "scary". Local anesthetics and proper pain medication allow the procedure to be
performed with little to no pain in most cases. There may be some soreness following the procedure, but that is normal
for most dental procedures. Over the counter painkillers are usually enough to relieve any pain afterwards, but your
dentist may prescribe medication. The procedure will also relieve you from pain caused by the infection allowing you to
enjoy all the foods you love without any pain from heat, cold, or biting too hard. If you are experiencing pain consult
your dentist today.

## Apicoectomy (Endodontic Surgery)

An apicoectomy is performed after an unsuccessful root canal. When an infection will not go away or returns after a root
canal has been performed this procedure is usually necessary. There are many nerves that may contain the infected
tissue, so it is difficult to ensure that all the infection is removed during a root canal. During an apicoectomy,
the tip of the root of the tooth is removed and replaced with a filling.

In most cases a second root canal is considered before an apicoectomy since it is a simpler, less invasive procedure.
Before the apicoectomy begins you will be given a local anesthetic to numb the area. The doctor will start by making an
incision in your gum to expose the root of your tooth. Any inflamed tissue will be removed to clean out the area. The
surgery takes place in a very small area, and only a few millimeters are removed from the root. For this reason, the
doctor will use magnification and small precision instruments to perform the surgery. The precise nature of the surgery
gives it a high rate of success. After the root is removed a filling is placed and the gums are sutured. Depending on
the type of sutures you may have to return in a few days to have them removed, or dissolving sutures may be used
instead. Over the course of the next few months the bone will heal around the root.

## Endo Microscope

The use of the Endo Microscope allows a doctor to perform procedures with greater accuracy because the treatment area is
magnified. The microscope's superior illumination and magnification has made the greatest impact on visualization of the
area, evaluation of surgical technique and use of fewer x-rays. With the unique ultrasonic unit and tips, a separated
instrument or silver point can be removed with less effort. Fourth canals are now routinely being located and completely
debrided in virtually all molars, thus increasing the long-term success rate with root canal therapy.

## Retreatment

With proper care, most teeth that have had endodontic (root canal) treatment can last as long as other natural teeth.
Root canals performed by endodontists (root canal specialists) have a 95% success rate. In some cases, however, a tooth
that has received endodontic treatment fails to heal. Occasionally, the tooth becomes painful or diseased months or even
years after successful treatment.

### Why do I need retreatment?

As occasionally happens with any dental or medical procedure, a tooth may not heal as expected after initial treatment
for a variety of reasons:

- Narrow or curved canals were not treated during the initial procedure.
- Complicated canal anatomy went undetected in the first procedure.
- The placement of the crown or other restoration was delayed following the endodontic treatment.
- The restoration did not prevent salivary contamination to the inside of the tooth.
- In other cases, a new problem can jeopardize a tooth that was successfully treated.

**For example:**

New decay can expose the root canal filling material to bacteria, causing a new infection in the tooth. A loose, cracked
or broken crown or filling can expose the tooth to a new infection. A tooth sustains a fracture. Retreatment is
performed in two visits and involves the following:

- At the initial visit the endodontist will examine the tooth, take x-rays and discuss your treatment options. If you
  and your endodontist choose retreatment, the retreatment will be scheduled at that time for a future date.
- At the retreatment appointment the endodontist will administer local anesthetic to numb the tooth. After the tooth is
  numb, the endodontist will reopen your tooth to gain access to the root canal filling material. In many cases, complex
  restorative materials (crown, post and core material) must be disassembled and removed to permit access to the root
  canals.
- After removing the canal filling, the endodontist can clean the canals and carefully examine the inside of your tooth
  using a microscope, searching for any additional canals or unusual anatomy that requires treatment.
- After cleaning the canals, the endodontist will fill and seal the canals and place a temporary filling in the tooth.
  Post space may also be prepared at this time.
- After your endodontist completes retreatment, you will need to return to your dentist as soon as possible to have a
  new crown or other restoration placed on the tooth to protect and restore it to full function.
- If the canals are unusually narrow or blocked, your endodontist may recommend endodontic surgery.

# Restorative

## Bridges

Dental bridges are a great way to replace missing teeth. Your existing teeth are used to literally create a bridge to
cross the area where your tooth is missing. Bridges are made from gold, metal, alloys, or porcelain to ensure that they
are strong and durable.

The process of creating a bridge begins by creating abutments out of your existing teeth where the bridge will be
attached. The existing teeth are recontoured to provide a base for the bridge. After the abutments have been created, a
mold is taken of the area which is sent to a dental lab. The lab is able to use the mold to create a bridge that will
fit properly and feel as close to your natural teeth as possible. The bridge consists of two crowns on either end to
place on the abutments and a pontic, which is the new tooth that replaces your missing tooth.

We will fit you with a temporary bridge while we wait for the lab to craft your permanent bridge. This will protect the
abutments and the exposed gum areas and look more appealing than having a missing tooth. When the permanent bridge has
been created, you will have a follow-up visit to set the bridge. It will be placed on the abutments and the dentist will
then use an adhesive to make sure that the bridge is set.

The bridge may take a little while to get used to, but after a few days it should feel like you have your own teeth back
again. You should eat soft foods for the first few days after having your bridge placed. After the initial phase, you
will be able to eat whatever you want with no issues.

If you are missing a tooth you should strongly consider having it replaced. Besides the aesthetic disadvantage of
missing a tooth, it could also cause structural changes to your mouth and jaw, as well as making it difficult to eat or
speak properly. Set up an appointment today to restore your smile.

## Crowns

Over time our teeth begin to weaken and become more susceptible to problems such as decay, cracks, discoloration and
others. If you feel your smile isn’t what it once was, crowns can help you recover your smile. If your dentist notices
that a tooth is decayed or seems weakened/cracked a crown may be necessary to make sure that there are no additional
problems with the tooth. In cases like this a filling or bonding will not be sufficient.

Crowns can be made from porcelain, porcelain fused to metal, or a full gold crown. To maintain a natural look and feel a
porcelain finished crown is best, as it can be matched to the shade of your other teeth. This will allow it to blend in
and appear just like one of your natural teeth.

The process of installing a crown takes 2-3 visits to the dentist. On the first visit the tooth will be reshaped by
filing down the enamel so that the crown can be placed over it. You will be given a local anesthetic before this part of
the procedure so that you do not experience any discomfort. Once the tooth has been reshaped, a mold will be taken of
that tooth and the surrounding teeth. This mold will be sent to a dental lab so that your new crown can be made so that
it fits in the spot created for it and looks the same relative to the surrounding teeth. Before leaving, your dentist
will fit you with a temporary crown until your permanent crown is ready.

The crown takes about 2-3 weeks to be returned to your dentist. At this time you will have another appointment to place
and fit the permanent crown. You will again have a local anesthetic to numb the area and the tooth will be placed using
a cement to ensure the tooth sets in place. When you look in the mirror, you will see your old smile back. Crowns are
durable and will usually last about 10-15 years. You should care for it as you would any of your other teeth with
regular brushing and flossing. Call us today if you would like to learn more about how crowns can help restore your
smile.

## Dentures

Dentures are a replacement for missing teeth that can be removed and put back into your mouth as you please. Depending
on each individual patient case, they may receive full or partial dentures. Full dentures are used when all the
natural teeth are removed from the mouth and replaced with a full set of dentures. There are two types of full dentures.

- Conventional Full Dentures - This is when all the teeth are removed and the tissue is given time to heal before the
  dentures are placed. It could take a few months for the gum tissue to heal completely, and during this time you will
  be without teeth.
- Immediate Full Dentures - Prior to having your teeth removed, your dentist takes measurements and has dentures fitted
  for your mouth. After removing the teeth, the dentures are immediately placed in your mouth. The benefit is that you
  do not have to spend any time without teeth. You will, however, need to have a follow-up visit to refit your dentures
  because the jaw bone will slightly change shape as your mouth heels. The dentures will need to be tightened after the
  jaw bone has healed. Partial dentures are another option when not all of your teeth need to be removed. This is
  similar to a bridge, but it is not a permanent fixture in your mouth.

Your dentures may take some time to get used to. The flesh colored base of the dentures is placed over your gums. Some
people say that it feels bulky or that they don't have enough room for their tongue. Other times the dentures might feel
loose. These feelings will affect the way you eat and talk for a little while. Over time, your mouth becomes trained to
eat and speak with your dentures and they begin to feel more and more like your natural teeth. They may never feel
perfectly comfortable, but it is much better than the alternative of not having teeth.

Even though dentures are not real teeth, you should care for them like they are. You should brush them to remove plaque
and food particles before removing your dentures. After they have been removed you should place them directly into room
temperature water or a denture cleaning solution. Never use hot water because it could warp the dentures. Your dentures
are delicate, so make sure you are careful when handling them so you don't drop them. Also, never try to adjust your
dentures yourself. You could ruin them, so you should always seek assistance from your dentist if they feel
uncomfortable or loose.

# Pediatric

## Sealants

Sealants are a great way to protect against tooth decay and cavities on your back teeth (molars). These are the teeth
that are most vulnerable to cavities and decay because they are used in the chewing process, and are the most difficult
to reach and clean. Molars first come in at around 5-7 years of age, with a second set coming in between the ages of
11-14. It is best to have a sealant placed when the molars first come in to ensure they are protected early.

To place a sealant an adhesive is first applied to the teeth. The sealant is then placed over the adhesive as a liquid,
as if it is painted right onto the tooth. The liquid then hardens and creates a barrier between your tooth and any
plaque, food particles, and bacteria. Sealants last for about 10 years and can be reapplied if necessary.

## Mouth Guards

Mouth guards are an essential piece of equipment in contact sports. They should be worn by athletes of all ages who
participate in sports such as:

- Football
- Hockey
- Lacrosse
- Wrestling
- Basketball
- Baseball/Softball
- Soccer
- ...any sport where contact is a possibility.

Wearing a mouth guard helps prevent against structural damage to your teeth and jaw and also helps prevent injuries such
as lacerations to your cheeks, tongue, and lips. Mouth guards have also been shown to help decrease the risk of
concussions.

Mouth guards are available in most any sporting goods store, but you should be careful when purchasing a mouth guard.
Mouth guards like this do not offer the best level of protection. They are also usually ill-fitting and uncomfortable.

For the highest level of comfort and protection, you should visit your dentist for a custom mouth guard fitting. A
custom mouth guard is created specifically for the optimal protection of YOUR mouth. It is created with thin plastic
that is hardened to protect your teeth. The thinness of the custom mouth guard allows for easy breathing and also allows
for easy communication (especially important for you star quarterbacks calling out the signals).

See your dentist today for a custom mouth guard to protect your teeth and ensure that you maintain the highest level of
performance on the field, court, or rink.

# Periodontic

## Arestin

#### Arestin: Fighting infection where it starts

You may have heard about gum disease, also known as "periodontitis" or "periodontal disease." Maybe a dental
professional or hygienist recently told you that you have this infection.

But do you really know the difference between periodontal disease and other types of complications that can affect your
mouth, such as gingivitis?

Do you know why it's so important to treat periodontal disease-and why brushing and flossing alone won't do the trick?

Most importantly, did you know that periodontal disease is today's #1 cause of tooth loss among American adults? Or
that, although a causal relationship between periodontal disease and an elevated risk for systemic events has not been
established,recent data suggest a possible association between periodontal disease and other health issues including
cardiovascular disease, diabetes, and preterm low birth-weight babies?

#### Fight infection right where it starts

ARESTIN® (minocycline hydrochloride) Microspheres, 1 mg is an effective antibiotic treatment that comes in powder form.
This powder is placed inside infected periodontal pockets just after the dental professional finishes the scaling and
root planing (SRP) procedure.

## Biopsy

Throughout the medical field, a biopsy is simply the removal of a tissue sample to determine if it is diseases. In
dentistry, teeth and gums are sent for biopsy. The role of a biopsy in the dental industry is to diagnose oral cancer.
In these instances, a brush biopsy is used to identify oral lesions that warrant further attention.

If you have unexplained lesions in your mouth, they need to be examined by a dentist. They may or may not be cancerous,
but they need medical attention nonetheless.

## Bone Grafting

Bone grafting is where the jawbone is built up to accommodate a dental implant or other restorative device. Bone
grafting is a common procedure that is used frequently for dental implants and other periodontal procedures. The bone
used to graft is taken from a sample from the patient. Many times, the bone is taken from another area of the mouth when
drilling takes place. The bone fragments are suctioned from the mouth and used for the graft. Cadaver bone fragments are
also used. They are harvested by bone banks and are a very safe source for bone donation.

### Ridge Augmentation

When you lose teeth, and do not replace them, the jawbone deteriorates where the tooth socket once was. This makes it
difficult, and in some instances impossible to get dental implants or dentures later on. You may have not had the
financial means at the time of the extraction for restorative surgery, but you may have the money now. The good news is
that we can perform a process called ridge augmentation to restore the bone structure that is needed for restorative
procedures such as dental implants. The process involves lifting the gum from the ridge to expose the defected area of
the bone. Then the dentist uses a bone like substance to fill the defected areas. The ridge augmentation greatly
improves the appearance of the mouth and increases the chances for success with the implants. With ridge augmentation,
your implants will last for years.

### Sinus Augmentation

Loss of posterior teeth may result in excessive forces being placed on your remaining teeth. Fortunately, the use of
dental implants and crowns allow you to replace these missing teeth. However, the position of the sinus in the upper
posterior areas may be too low for proper placement of dental implants.

A simple procedure allows the sinus floor to be repositioned, creating enough space to properly place an implant.
Various grafting materials are used to encourage your bone to grow more quickly into the area, helping to stabilize the
dental implant. Replace with your own bone in this area the grafting material as it grows into the area.

Under certain conditions, an even simpler procedure can be utilized. When possible, the bone remaining under the sinus
floor is gently “pushed up”, thus lifting the floor of the “dropped” sinus. Bone replacement materials are then placed
beneath this lifted bone. Once again the bone materials are replaced as your body grow new bone into this area.

Sinus augmentation procedures are highly predictable, with studies reporting over 95% success. Following sufficient
healing of a sinus augmentation (6-10 months), implants are placed in a predictable and successful manner. It is
important to realize that if the sinus augmentation procedure does not result in enough bone for implant placement,
additional bone may be regenerated through a second sinus augmentation procedure at the time of implant placement.

## Canine Exposure for Orthodontics

Canine exposure has nothing to do with leaving your dog outside, exposed to the elements. It is a procedure to expose
impacted teeth. An impacted tooth is one that has not erupted in the mouth, but instead becomes stuck in the surrounding
bone or tissue. Any tooth may become impacted, but generally, the wisdom teeth and canine teeth are the most likely
candidates. Canine teeth are critical for function. The mouth will also appear aesthetically odd without the canines.
The procedure is quite simple. It involves the dentist cutting a small hole in the gum, which allows the tooth to erupt.
The dentist also uses a dental brace to guide the tooth into its correct position.

Impacted teeth are sometimes obvious to the naked eye, but in some instances, an –x-ray is necessary to identify the
extent of the impaction. Many times, there is an impacted canine tooth where the baby tooth remains in the mouth. A
loose tooth is also a sign that an impacted tooth may be present.

## Crown Lengthening

It is no secret that dentists are committed to saving teeth. This is why we fill a cavity, instead of pulling the tooth.

Cavities can decay to tooth to the point where restoration is virtually impossible without a procedure called crown
lengthening. Crown lengthening is a routine surgical procedure, which remodels the contour of the gum line. The
procedure does not actually lengthen the crown, but rather lowers the gum line. When there is not enough tooth structure
to affix a crown, this is the only option. Sometimes a tooth has been broken below the gum line. In this instance, crown
lengthening is very successful in exposing more of the tooth, so that the dentist has something to work with.

## Frenectomy

A frenulum is a piece of tissue that prevents an organ from moving. There is a frenulum that attaches your upper lip to
the gums, while another connects the lower lip to the gums. A frenulum that is too short or thick, will cause problems
in speech patterns and tooth misalignment. In infants, a shortened frenulum underneath the tongue will inhibit
breastfeeding. When the frenulum disrupts movement, growth, or development, corrective action is necessary to resolve
the situation.

A frenectomy is a minor surgical procedure that is performed in your dentist’s office. It can be performed with either a
scalpel or laser and takes less than 15 minutes. Using a laser causes very little bleeding and does not require
stitches. A laser also results in less postoperative discomfort and a shorter healing time. Young children and infants
are put under general anesthesia for the procedure and adults have the procedure performed using local anesthesia. If
your child needs a frenectomy, there is nothing to worry about. The procedure is very successful and causes minimal
discomfort.

## Gingival / Gum Grafting

Gingivitis and in its advanced state, periodontitis, has a profound effect on the gums. As gingivitis progresses, more
and more bacteria and plaque builds up, causing the gums to stretch. The end result it large pockets, that once they are
cleaned out, remain on your gum line. These pockets cause the gums to recede, which aesthetically not pleasing to the
eye. When the gums recede, an abnormal amount of tooth structure is exposed.

Gum grafting is the corrective procedure that restores the gum to its natural, healthy state. Using soft gum tissue from
the roof of the mouth, the receded gums are grafted. The goal if the graft is to cover exposed tooth and root surfaces
with grafted on oral tissue. This grafting encourages new tissue growth that will enable the gums to return to its
original position around the teeth. The procedure is routine and entails a minimal amount of downtime and discomfort.

## Occlusal Adjustment

**Do you wake in the morning with sore jaws?**

When you bite, do you feel like your jaw is lopsided? If so, then you may need an occlusal adjustment.

An occlusal adjustment corrects the alignment of the bite, that is a result of loose, shifting, crowded, or missing
teeth. The result is an evenly distributed bite that eliminates irregular pressure on one side of the mouth. Once your
bite is adjusted, your teeth will meet properly. Occlusal adjustment causes minimal pain, and only a little discomfort.
The adjustment is made by using a dental drill using a fine filing stone. In addition to the actual adjustment, removal
mouthpieces are also utilized, to protect the tooth surface, and relax the jaw muscles once the adjustment is completed.

Who is a good candidate for an occlusal adjustment? Patients with loose or shifting teeth will many times not meet
correctly. Patients, who grind or clench their teeth, will have an uneven bite and pressure distribution in the mouth,
which is also corrected through an occlusal adjustment. Sometimes tooth sensitivity can be corrected through an occlusal
adjustment as the treatment reduces pressure on the sensitive tooth.

New technology allows dentists to accurately identify the areas, which need adjustments. The dentist utilizes a computer
scan of the mouth, which records hundreds of bite registrations per minute, and notes even the slightest irregularity.
That data allows the dentist to make only the adjustments that are absolutely necessary, which ensures a well aligned
bite and minimal tooth wear.

If you suspect that you may need an occlusal adjustment, schedule an appointment.

## Periodontal Splinting (Weak Teeth)

Loose teeth are uncomfortable, especially when you try to eat food or chew gum. The feeling of the tooth pulling away
from the gum is enough to send chills down your spine. It seems like an eternity, waiting for either the tooth to become
loose enough to be extracted or strong enough to no longer be a problem.

Teeth become loose because of lost gum tissue, injury, orthodontic treatment, or pressure caused by tooth misalignment.
A new technique called periodontal splinting attaches weak teeth together, turning them into a single unit that is
stable and stronger than the single teeth by themselves. The procedure is most commonly performed on the front teeth.
The procedure is as simple as using composite material to attach, or splint, the loose teeth to the adjoining stable
teeth. Tooth splinting is a common procedure that has gained popularity due to its effectiveness.

Life is too short to live with loose teeth.

Contact our office today for a consultation.

## Osseous Grafting / Guided Tissue Regeneration

Osseous surgery is a procedure that reshapes the bone which holds your teeth in place. Osseous surgery commonly treats
periodontitis. Patients with periodontitis experience defects in the bone around their teeth. The osseous surgery
removes those defects. Prior to the surgery, the patient undergoes a periodontal treatment that consists or scaling and
root planing. A local anesthetic is administered to minimize the pain. Once the roots are cleaned, the dentist uses a
drill and sharp dental tool to reshape the bone surrounding the teeth. Depending on the extent of the defects, the
deformed bone is removed, and the rest is shaped. Bone grafting material is used where the defects are too large to be
treated with only reshaping. Once the bones are back to their original state, the gums are stitched back into place.
Osseous Grafting is a routine procedure, with a high success rate.

## Cosmetic Periodontal Surgery

Your smile is the first thing someone notices about you. People form their first impressions based on the appearance of
your smile. There was a time when, unless you made allot of money or were born with perfect teeth, you had to live with
your smile. Today, a wide range of cosmetic procedures is available to the average citizen, at a cost they can afford.
If you have a gummy smile, uneven gum line or elongated teeth, cosmetic periodontal surgery is for you.

Cosmetic periodontal surgery sculpts the gum line so that it is even and in proportion to the amount of exposed tooth
versus gum. This procedure removes the excess gum and exposes more of the tooth crown. If your gums have receded, and
your teeth appear overly long, then soft tissue grafts can extend the gum line to create an aesthetic balance. The
grafts also reduce the gum pockets that are prone to future periodontal disease.

If you are unhappy with your smile, give us a call to discuss your options. After all, you only have one chance to make
a first impression.

## Periodontal (gum) disease

Periodontal (gum) disease is insidious. It is an infection of the gums that starts out as plaque, an opaque film on the
teeth that hardens to form tartar. As tartar accumulates, it harbors bacteria that attack the soft tissue around the
gums. This is the early stage of gum disease known as Gingivitis. Left untreated, Gingivitis becomes Periodontitis which
ultimately destroys the tissue surrounding your teeth AND the bone that holds your teeth in place. Except for bad breath
and gums that bleed, there are very few early warning signals. The disease advances silently, often without pain, and
before you know it, you are losing your teeth and you don't know why.

Tooth loss is only the most obvious indicator of gum disease. Scientific research has discovered linkage between gum
disease and stroke, heart disease, diabetes - even an increased risk for pregnant women. When your gums become diseased,
your entire immune system is weakened.

In the past, fear of painful dental surgery has kept people with gum disease from seeking the care they needed. Well,
those days are gone forever.

## Scaling & Root Planing

Gingivitis is a generative disease that left untreated, will cause significant tooth and gum deterioration. Just the
word gingivitis can strike panic in a patient’s mind. The reality is that the treatment is simple and performed right in
your dentist’s office.

Plaque and tarter that sits on the teeth provides an environment, which allows bacteria to thrive and multiply. The
bacteria cause the gums to become inflamed and bleed. The condition becomes more noticeable when you brush your teeth or
sometimes when you eat. These are signs of the early stage of gingivitis. Gingivitis is easily treated by having the
hygienist scaling and polishing the teeth. If gingivitis is left untreated, the condition will progress and the roots
will need a planing. The difference between scaling and root planing is simple. Scaling is the removal of the dental
tartar from the tooth surface Root planing is the process of smoothening the root surfaces and removing the infected
tooth structure.

As a non-surgical procedure, scaling and planing is performed without any anesthesia, in the dentist’s office. While the
procedure is usually painless, advanced stages of gingivitis may make it necessary to numb the area for complete
comfort. Deep scaling and root planing is usually broken down into one section of the mouth per appointment. This allows
for adequate healing time, and reduces the time for each appointment.

## Soft Tissue Graft

A soft tissue graft is used when there has been a significant amount of gum recession in a particular area. Slight gum
recession can usually be fixed with a few changes to your oral hygiene routine to take better care of your teeth and
gums. When the gingiva recedes further it exposes you to greater risk for infection and bacterial penetration. You will
likely be more sensitive to hot and cold foods when you have receding gums. If the gums recede enough as to expose the
root you can set yourself up for more serious problems. The root is softer than the enamel making it more vulnerable to
bacteria and plaque.

To restore proper gum level and functionality a soft tissue graft can be performed. This is done by either removing soft
tissue from the roof of the mouth, or repositioning healthy gum tissue from adjacent teeth. This procedure is very
predictable and has a high success rate. This procedure should be performed before more serious problems develop and
periodontal surgery is necessary.

## Osseous Surgery / Pocket Reduction

Sometimes the effects of periodontal disease create permanent changes in the tooth and gum structure that will cause
issues in the future. Enlarged gum pockets between the tooth and the gum line are common after having advanced gum
disease. Sometimes these gaps are cosmetic in nature, and affect the appearance of the gums. More commonly, the gaps put
the teeth at future risk for tooth and gum disease, as they are just one more place that plaque and bacteria can
collect. Pocket reduction surgery is designed to thwart the after effects of periodontal disease and restore your mouth
to a healthy state.

The goal of periodontal surgery to gain access to the tooth root and to clean the damaged areas. Once the dentist can
visually see the damage, it can be removed completely. Removing the plaque and decayed gum tissue leaves a pocket
between the gum and the tooth. Sometimes the gum returns to its original position, but still the pocket is present. The
pocket requires more frequent cleanings as the patient is unable to get to the pockets with regular brushing and
flossing. Once the swelling from the periodontal treatment has subsided, the dentist may need to suture the gum to where
the bone has resorbed. The goal is to create a space large enough so it can be reached through daily oral hygiene, but
small enough that it is not a breeding ground for plaque and bacteria.

# Oral Surgery

## Wisdom Teeth

Wisdom teeth are the third and final set of molars that emerge, usually during your late teens to early twenties. For
some people the wisdom teeth emerge through the gums and have enough room to grow in naturally. For others, wisdom teeth
often cause problems as they are trying to protrude through the gums. When a wisdom tooth is impacted the tooth is
coming in at an angle and not straight through the gum line. This can cause pain, the tooth can come in unevenly, or the
tooth may only emerge partially.

Impacted wisdom teeth can cause structural damage to the jaw and other teeth. They can also provide a place for bacteria
to gather since they are hard to reach and clean. These potential problems make it necessary to remove impacted wisdom
teeth so that larger problems do not arise. Routine x-rays during a dental exam can reveal if you will need to have your
wisdom teeth removed.

## Extractions

Wisdom teeth extractions are a fairly common procedure. Wisdom teeth often cause problems as they are trying to protrude
through the gums. When a wisdom tooth is impacted, it means the tooth is coming in at an angle and not straight through
the gum line. This can cause pain, the tooth can come in unevenly, or the tooth may only emerge partially.

When a wisdom tooth only emerges partially a flap of skin, called an operculum, may form over the tooth. This can make
the tooth hard to clean, and pieces of food may be caught under the skin. This makes it easy for an infection, called
pericoronitis, to develop. It will usually go away on its own, but it causes swelling and pain in the area.

Impacted teeth and wisdom teeth that can potentially cause problems, like infections, need to be removed. Extractions
can range from a single tooth, to removing all four wisdom teeth at once. Based on the preference of the doctor and/or
the patient, a local anesthetic could be used to numb the areas where the teeth will be extracted. Others will prefer to
go under a general anesthetic so that they will be sedated during the procedure.

The gum tissue around the wisdom tooth is cut open to reveal the tooth. The tooth is loosened by gripping it tightly and
wiggling it back and forth until it can be lifted out of the gums. Sometimes a tooth may be impacted so tightly that it
cannot be simply lifted out of the gums. In cases like this the tooth will be broken up into pieces first before being
removed. Depending on the incision and extraction site, sutures may be needed to close the area. Soluble sutures are the
best option, which will dissolve on their own.

After the surgery you will need to rest. You need to be driven home by a friend or family member because of the
anesthesia. You can expect for the extraction site to bleed for a little while after the surgery. Gauze will be applied
at the completion of the surgery, and you will need to change it when it becomes soaked. If bleeding continues for
longer than 24 hours you should call your dentist. Rest when you return home, but do not lie flat. This could prolong
the bleeding. Prop your head up on a pillow when lying down. Your dentist will prescribe you pain medication, so if you
become sore take as directed. You can also use an ice pack for the pain. Your dentist might also provide you with a
cleaning solution to clean the extraction site.

You will be limited to soft foods for a few days after your surgery. Some recommended foods are:

- Gelatin
- Pudding
- Yogurt
- Mashed Potatoes
- Ice Cream
- Thin Soups
- ... and other food you can eat without chewing.

When drinking, make sure you do not use a straw. The sucking motion can loosen your sutures and slow the clotting
process. The same goes for smoking. If you have prolonged pain, bleeding, irritation, or don't feel that the extraction
site is healing properly call your dentist for a follow-up.

## Extraction Site Preservation

When removing a tooth it is important to consider what will be done with the empty space after that tooth is removed.
Wisdom teeth are in the back of the mouth, so that site will heal on its own with no complications. If it is necessary
to remove another tooth, plans must be made. If a tooth is removed and nothing is done with the extraction site, the jaw
bone will degenerate and change shape during healing and can cause your teeth to shift. This can create problems in your
bite and affect your ability to speak and chew.

If you want to fill the space with a dental implant, a sturdy jaw bone is necessary to install the implant. If you opt
for a dental bridge, the bridge must be molded and placed before the teeth shift.

Your dentist is always open to a conversation on what you would like to do with your extraction site before removing a
tooth. They will be able to make a recommendation and layout a treatment plan. Make sure to schedule follow up
appointments to properly care for your extraction site.

## Bone Grafting

Bone grafting is where the jawbone is built up to accommodate a dental implant or other restorative device. Bone
grafting is a common procedure that is used frequently for dental implants and other periodontal procedures. The bone
used to graph is taken from a sample from the patient. Many times, the bone is taken from another area of the mouth when
drilling takes place. The bone fragments are suctioned from the mouth and used for the graft. Cadavers bone fragments
are also used. They are harvested by bone banks and are a very safe source for bone donation.

## Oral Pathology

Oral Pathology is the specialty that identifies and treats diseases of the mouth and maxillofacial region. Diagnosis is
completed through radiographic, microscopic, biochemical and other in office examinations. Oral pathologists provide
biopsy services for dentists and offer clinical their diagnosis based on their findings. Some of the diseases that Oral
pathologists diagnose include mouth and throat cancer, mumps, salivary gland disorders, ulcers, Odontogenic Infection,
and others.

## Sleep Apnea

Using oral surgery to help sleep apnea seeks to remove the excess tissue in the throat that is vibrating and blocking
the upper air passages. One surgical procedure is an Uvulopalatopharyngoplasty (UPPP). This procedure involves removing
the excess tissue from the upper mouth and throat. This procedure in performed in a hospital under general anesthesia.
Maxillomandibular advancement is another type of procedure used to assist with sleep apnea. This procedure involves the
upper and lower part of the jaw. In this procedure, the jaw is moved forward from the rest of the facial bones. This
allows more room behind the soft palate, thereby reducing the obstruction. Finally, a Tracheostomy is a last ditch
effort when other treatments have failed. This involves the surgeon inserting a tube in your throat so you can breathe.
It is covered during the day, but opens at night while you sleep. All of the aforementioned surgeries are routine and
very safe.

# Orthodontic

## Clear Correct

_Clear Correct_ is a new system of straightening teeth without the use of conventional braces. A series of clear plastic
aligners are utilized to create tooth movement. Moving teeth with removable aligners is not new. However, the computer
program, which can generate a series of aligners with small changes is the new part. Clear Correct is recommended for
orthodontic situations with mild to moderate spacing or crowding. They are virtually undetectable, easy to use and
comfortable to wear.

## Clear Braces

Whether you're an adult or a teen, we all want to look our best at all times. Changing the way your teeth are structured
takes time and there are now ways to keep you looking your best during this period. Clear braces or ceramic braces are
translucent. This new technology is available for most cases. The great benefit of these braces is that you won't have
to have that metallic look. In addition, clear braces are specifically designed so that they won't stain or wear over
time.

Give us a call today to see if you're a candidate for ceramic braces!

## Night Guards

Many people are afflicted with bruxism, or teeth grinding. Some people may do this consciously during the day, but it is
a larger problem at night while you are asleep. Grinding your teeth can damage enamel, wear down teeth, cause jaw pain,
or irritate your gums. The noise from teeth grinding can also disturb your spouse's sleep if loud enough.

If you grind your teeth you should consider a night guard. The night guard, which is very similar to a mouth guard worn
by athletes, provides a barrier between your top and bottom teeth while you sleep. All night guards are custom fitted
for comfort and to allow for proper breathing. Your dentist will take an impression of your teeth and have the night
guard created by a dental lab. Night guards are very durable and can be used for up to 10 years.

There are also some things that you can do to try to stop teeth grinding. You can train your jaw to be free and easy
rather than clenched. Refrain from chewing gum or on other objects like pens. You should also avoid alcoholic drinks and
drinks with caffeine, as these can increase the likelihood you will grind your teeth. If you suspect you might be
grinding your teeth at night set up an appointment with us today.

## TMJ

TMJ is the acronym for temporomandibular joint, which connects your lower jaw (the mandible) to your skull at the
temporal bone. This joint controls many jaw functions, like chewing. If the chewing muscles or the joint itself are
causing you pain you may have temporamandibular disorder, or TMD. TMD can be caused by stress, continual clenching of
the jaw muscles, or teeth grinding.

Symptoms of TMD include:

- Pain when opening or closing mouth
- Trouble chewing
- Jaw becoming stuck open or shut
- Headaches or ear pain
- Clicking or popping sounds when opening your mouth
- Teeth Grinding

Many of these symptoms can often be associated with other health problems, so only a medical professional can tell you
if it is due to TMD. Teeth grinding is an especially problematic symptom because it can lead to further problems.
Prolonged teeth grinding, or bruxism, can cause enamel to wear off teeth and expose dentin. This material is softer than
enamel and more susceptible to decay. Sensitivity to hot and cold food or drink may also develop from excessive teeth
grinding.

If you suspect you may have TMD come in for a consultation. We can help diagnose you and provide relief for your
symptoms. Pain relievers and hot/cold compresses are short term methods to provide relief for pain symptoms. A night
guard can be used to help prevent or lessen the effects of teeth grinding at night. This can lead to a more permanent
solution. In very severe cases of TMD surgery may be required, but behavioral treatments to change the way you use your
jaw muscles are usually enough to provide relief.
