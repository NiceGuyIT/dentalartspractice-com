Staff Biographies

## Marie

Hello, my name is Marie, I grew up in Los Angeles, CA. I began working in the dental field in 1988.l gained my
experience working alongside a doctor, as a dental assistant. When I am away from my friends at the office; I enjoy
spending time with my husband, 4 children, and 2 grandchildren. On the weekends, I enjoy going to football games and
relaxing under the warm sun.

## Sarai

My name is Sarai, l was raised in Covina, Ca. My hobbies are going on peaceful morning hikes and looking after my fish
tank. I have been in the Dental field for 2 years, working as a dental receptionist. After work, you will catch me
driving to school, pursuing a major in Business Administration, and catching up on my mid—day siesta.

## Nadia

Hi, my name is Nadia, l was raised in East Los Angeles. l have been working in the dental field for 7 years and have
enjoyed working and making morning coffee for Dr. Nazari. On my spare time when l’m not billing, I love making my
coworkers laugh and bringing life to the quiet office. Lastly, my pride and joys are my 3 children, my 2 grandchildren,
and my 3rd grandchild on the way. GO DODGERS!!!

## Monica

Hi, my name is Monica Paredes. I am a Dental Assistant and have been working in the Dental Field for 26 years. I love
and enjoy helping Dr. Nazari with all the patients that visit us here at Dental Arts Practice. When I am not working, I
love to spend time with my son and 2 grandchildren, and the rest of my family. My favorite hobby is taking my
grandchildren to the beach and laying under the sun!

## Jess

My name is Jessica, i am one of the dental assistants who works alongside Dr. Nazari. I’ve been in the dental field for
a year now. While on my days off, I love to go on Adventures such as going off—roading, shooting-range, and taking long
walks with my friends and family. Anything adventurous always catches my attention. My favorite teams are the Lakers,
Dodgers, and Cowboys!!! (Yes, I know... There’s always next year. lol)

## JoAnn

Hello, my name is JoAnn. I am the Office Manager here at Dental Arts Practice. I have been working in the Dental FieId
for 26 years. When I began working with patients, I took on the position as a Dental Receptionist, where I progressed
into billing, and later took on the responsibility of being an Office Manager. Aside from managing a business, I enjoy
hiking and weightlifting on my free time. Occasionally on the weekends, I also enjoy going to concerts with brothers and
listening to Rock and Roll music. Most importantly, my favorite time of the day is the time I spend with my fiancé, my
two sons, and my dog (FIF).

## Ben

Hi my name is Benjamin, but they call me biscuit for short. I was raised in Oxnard, and I am new to the dental field. I
work with the most amazing, high energy coworkers, they are truly the best. I would not change that for anything. Aside
from working, I like to spend my days with my wife and 4 dogs. On my days off, I like to listen to music, play video
games, and go on adventures with my family.

# New


## May

Hello, my name is May Sato-Zacher. I graduated from USC with a Bachelor of Science in
Biology and a Bachelor of Science degree in Dental Hygiene. Early in my career I helped to
teach at USC, and currently I teach dentists and their staff at surgical seminars. I am certified in
using dental lasers and dental periscope. I find it very rewarding mastering new concepts and
skills in dentistry.

I have been in practice for over 30 years, and I am very dedicated to my patient’s dental health
care! I make sure my patients feel comfortable and at ease, so they have a pleasant dental
experience. I Love God, my family and friends and my patients are family to me!

## Diana

Hello, my name is Diana Flores. I have been a Dental Hygienist since 2003. However, I have
been in the dental field for nearly 30 years. I have a passion for promoting dental health and its
link to your overall systemic health. When I am away from the office, I enjoy spending time with
my family and friends, and my 2 basset hounds. My two boys keep my husband and I busy with
sports. Dental Arts Practice has been a true blessing to me since I started working here in 2006.

## Becky

Hello, my name is Rebecca (Becky) Zwingman. I have been a Dental Hygienist since 2016 but
have been in the dental field since 2005. My passion is to educate my patients and students. I
love seeing things click for them, having that “ah—ha” moment when it comes to
brushing, flossing and the changes in their oral health. Also, I pride myself on continuing my own
education to ensure I am up to par with the newest in Dentistry and education.

My education consists of a Masters in Education, Bachelor’s in HeaIth Sciences, Associates in
Dental Hygiene and an Associates in Dental Assisting. I currently hold multiple professional
licenses: Registered Dental Assistant, Registered Dental Hygienist and Registered Dental
Hygienist in Alternative Practices. I decided to continue my RDH in alternative Practice to allow
me to go to patients and treat those who can't physically come to the dental office.

I am currently a Dental Hygiene full time and work for Dr. Nazari 2 Saturdays a month. Although
my heart lies with clinical hygiene, I really love education and developing individuals who have
the passion for Dentistry. This allows me to have a bigger impact in dentistry by molding
students into great clinicians who will then go out and treat patients.

Some personal Hobbies & passions of mine are baking, cooking (currently have a small
business called “Sweet Treats”), traveling and helping raise my nephew and niece.

## Wendy

Hello, my name is Wendy Pena. I graduated from San Joaquin Valley College. I have a degree
in Dental Hygiene. It has been with great honor to join Dr. Nazari and his team. I enjoy mostly
working with fellow dental health care professionals that share my same passion in providing
the best quality patient care. I began as a dental assistant in 2019, and I am now pleased to be
a Dental Hygienist making sure the patients always have a comfortable and positive experience.
I am very passionate about treating patients with sensitivity to their dental needs. I consider
myself a lifelong learner.

Outside the office, I enjoy spending time with my family and going out and seeking adventurous
things to do from trips, to exploring new places.
